#!/usr/bin/env python3

import sys

from Crypto.PublicKey import ECC
from Crypto.Signature import DSS
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
from cryptography.hazmat.primitives.asymmetric import ec
import base64
from json import dump


def helper(arg):
    helper_str = f"""
        {arg} [-g] 
        [-s <private_key> <data>] 
        [-w <filename>] 
        [-cc <secret> <private_key> <public_key>]
        [-sc <challenge> <private_key> <public_key>]

    Options:
        -g Generate pair of keys (private and public in a hex view)
        -s Sign provided line using private key

            -s <private_key> <data>

        -w Write result in provided filename

            -w <filename>

        -cc Create a challenge for provided secret using responder's 
            of a friendship request private key and initiator's public key

            -cc <secret> <private_key> <public_key>

        -sc Solve a challenge using provided initiator's private key 
            and responder's public key of a friendship request 

            -sc <challenge> <private_key> <public_key>
    """
    return helper_str


def generate_pair_of_keys():
    """
    Generate keypair for user
    """
    sk = ECC.generate(curve="P-256")
    pk = sk.public_key()
    private = sk.export_key(format="DER").hex()
    public = pk.export_key(format="DER").hex()
    # print(f"private key:{private}\npublic key:{public}")
    return private, public


def sign_line(private_key: str, for_signing: str):
    """
    Sign given line by private key
    :return: signed line
    """
    sk = ECC.import_key(bytes.fromhex(private_key))
    signer = DSS.new(sk, mode="deterministic-rfc6979", encoding="der")
    h = SHA256.new(for_signing.encode())
    signed_line = signer.sign(h)
    # print(signed_line.hex())
    return signed_line.hex()


def sha_ecdh(private: ECC.EccKey, public: ECC.EccKey) -> bytes:
    """
    Compute SHA256(ECDH(private, public))
    :return: shared key
    """
    sk = ec.EllipticCurvePrivateNumbers(
        int(private.d),
        ec.EllipticCurvePublicNumbers(
            int(private.pointQ.x), int(private.pointQ.y), ec.SECP256R1()
        ),
    ).private_key()
    pk = ec.EllipticCurvePublicNumbers(
        int(public.pointQ.x), int(public.pointQ.y), ec.SECP256R1()
    ).public_key()
    dh = sk.exchange(ec.ECDH(), pk)
    # print(f"dh:{dh.hex()}")
    key = SHA256.new(dh)
    # print(f"key:{key.hexdigest()}")
    return key.digest()


def create_challenge(secret: str, r_private: str, init_public: str) -> str:
    """
    Create challenge base64(AES_ECB(key=SHA256(ECDH(private_init, public_resp)), secret)))
    :param secret: secret for challenge
    :param r_private: responder's private key
    :param init_public: initiator's public key
    :return: encoded challenge
    """
    private = ECC.import_key(bytes.fromhex(r_private))
    public = ECC.import_key(bytes.fromhex(init_public))
    key = sha_ecdh(private, public)

    cipher = AES.new(key, mode=AES.MODE_ECB)
    encrypted = cipher.encrypt(pad(secret.encode(), 16))
    challenge = base64.b64encode(encrypted)
    return challenge.decode()


def solve_challenge(challenge: str, init_private: str, r_public: str) -> str:
    """
    Solve challenge base64(AES_ECB(key=SHA256(ECDH(private_init, public_resp)), secret)))
    :param challenge: encoded challenge
    :param init_private: initiator's private key
    :param r_public: responder's public key
    :return: decoded secret
    """
    private = ECC.import_key(bytes.fromhex(init_private))
    public = ECC.import_key(bytes.fromhex(r_public))
    key = sha_ecdh(private, public)

    encrypted = base64.b64decode(challenge.encode())
    cipher = AES.new(key, mode=AES.MODE_ECB)
    secret = cipher.decrypt(encrypted)
    return unpad(secret, 16).decode()


if __name__ == "__main__":

    try:
        if len(sys.argv) == 1:
            print(f"You can use for a help: {sys.argv[0]} -h")

        args = sys.argv[1:]
        result = {}

        if "-h" in args:
            print(helper(sys.argv[0]))

        if "-g" in args:
            private, public = generate_pair_of_keys()
            result["private"] = private
            result["public"] = public

        if "-s" in args:
            signed_line = sign_line(
                args[args.index("-s") + 1], args[args.index("-s") + 2]
            )
            result["signed_line"] = signed_line

        if "-cc" in args:
            arg_pos = args.index("-cc")
            challenge = create_challenge(
                args[arg_pos + 1], args[arg_pos + 2], args[arg_pos + 3]
            )
            result["challenge"] = challenge

        if "-sc" in args:
            arg_pos = args.index("-sc")
            secret = solve_challenge(
                args[arg_pos + 1], args[arg_pos + 2], args[arg_pos + 3]
            )
            result["secret"] = secret

        if "-w" in args:
            filename = args[args.index("-w") + 1]
            with open(filename, "a") as fd:
                dump(result, fd)
            print(f"saved to provided file: {filename}")

        else:
            for k in result:
                print(f"{k}: {result[k]}")

    except ValueError:
        print(helper(sys.argv[0]))
    except Exception as e:
        print(f"Error occurred: {e}")
