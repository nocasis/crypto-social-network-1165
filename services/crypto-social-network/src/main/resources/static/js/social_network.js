/**
 * Create token storage closure
 */
var tokenStorage = function() {
    var accessToken = null;
    return {
        /**
         * Set token variable
         * @param {String} token    access token
         * @return {null}
         */
        setToken: function(token) {
            accessToken = token;
        },
        /**
         * Get token variable
         * @return {String} token   access token
         */
        getToken: function() {
            return accessToken;
        }
    };
}();



// Patch original fetch function to include auth header
var defaultFetch = fetch;
var current_id;
var my_id;
var users_table;
var requests_table;
var privateKey_button = '<p class="text-privateKey">Добавить приватный ключ</p><p><input type="text" placeholder="Введите свой новый приватный ключ" id="privateKey" required></p><p><input id="privateKey-button" class="dark" type="submit" value="Сохранить"></p>';
var load_img_button ='<form id="avatar-form"><p class="avatar-text">Загрузить новую аватарку?</p><input id="avatar-file" type="file" name="file"/><br><br><button id="avatar-button" type="submit">Загрузить</button></form>'
var curr_img;
var avatar = "";
var tokens_for_invite = new Map();

var profileInfo = function(json1) {

    profile_info = '<p>id: '+json1.user.id+'</p><p>login:</p>'

    if (json1.user.login.length > 55){
        var modulo = Math.floor(json1.user.login.length/55)
        for (let i=1; i<json1.user.login.length/55;i++){
            profile_info += '<p>'+json1.user.login.slice((i-1)*55,55*i)+'</p>'
        }

        profile_info += '<p>'+json1.user.login.slice(55*modulo,)+'</p>'
    }
    else{
        profile_info += json1.user.login
    }

    profile_info +='</p><p>publicKey:</p>'

    if (json1.user.publicKey.length > 55){
        var modulo = Math.floor(json1.user.publicKey.length/55)
        for (let i=1; i<json1.user.publicKey.length/55;i++){
            profile_info += '<p>'+json1.user.publicKey.slice((i-1)*55,55*i)+'</p>'
        }

        profile_info += '<p>'+json1.user.publicKey.slice(55*modulo,)+'</p>'
    }
    else{
        profile_info += json1.user.publicKey
    }

    profile_info += '<p>friendsIds: '+json1.user.friendsIds+'</p>';

    if (json1.user.avatar.length){

      const options = {
          method: 'GET',
          headers: {
              'Content-Type': 'application/json',
          },
      };
      fetch('/profile/avatar/?id='+current_id, options)
          .then((res) => res.json())
          .then((json) => {
              if (json.status == 'ok'){
                  avatar = json.message;
              }
          });
    }

    return profile_info;
}

var postsInfo = function(json, token='', secret='') {

    table_posts = '';

    if (json.posts.length){
      table_posts += '<table class="table"><tr><th>postid</th><th>ownerId</th><th>theme</th><th>text</th><th>reportsCount</th><th>private</th><th>report</th><th>token+secret</th></tr>';

      for (item of json.posts) {
          table_posts += '<tr><td>'+item.postId+'</td> <td>'+item.ownerId+'</td> <td>'

          if(item.theme.length > 25) {
              var modulo = Math.floor(item.theme.length/25)
              for(let i=1;i<item.theme.length/25;i++){
                  table_posts += '<p>' + item.theme.slice((i-1)*25,25*i)+'</p>';
              }
              table_posts += '<p>'+item.theme.slice(25*modulo,) + '</p>'
          }
          else{
              table_posts += item.theme;
          }

          table_posts += '</td><td>'

          if(item.text.length > 25) {
              var modulo = Math.floor(item.text.length/25)
              for(let i=1;i<item.text.length/25;i++){
                  table_posts += '<p>' + item.text.slice((i-1)*25,25*i)+'</p>';
              }
              table_posts += '<p>'+item.text.slice(25*modulo,) + '</p>'
          }
          else{
              table_posts += item.text;
          }


          table_posts += '</td> <td>'+item.reportsCount+'</td> <td>'+item.private+'</td><td><p><a href="#" onclick="postReport('+item.postId+')">Репортнуть пост</a></p></td>'

          if (item.private == true){
              table_posts += '<td><a href="#" onclick="getTokenAndSecret('+item.postId+')"><p>Получить токен и секрет для</p><p>чтения данного поста</p> </a></td>'

          }

          table_posts += '</tr>';
      }
      table_posts += '</table>'
      if (token != '' && secret != '') {
          table_posts += '<p>токен: '+token +'</p><p>секрет: '+secret+'</p>'
      }
    }

    return table_posts;
}

var getTokenAndSecret = function(id){
    var options = {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                fetch('/profile/post/private/?id='+id,options)
                    .then((res) => res.json())
                    .then((json) => {
                          if (json.status === 'ok') {
                              const token = json.token;
                              const secret = json.secret;
                              var options = {
                                  method: 'GET',
                                  headers: {
                                      'Content-Type': 'application/json',
                                  },
                              };
                              fetch('/profile/?id='+current_id,options)
                                  .then((res) => res.json())
                                  .then((json) => {
                                      containers_data = [];
                                      containers_data.push(profileInfo(json));
                                      containers_data.push(postsInfo(json,token,secret));
                                      if (current_id == my_id) {
                                          containers_data.push(privateKey_button);
                                          containers_data.push(load_img_button);
                                          loadPage(page = 'profileForm', method = 'GET', prefix ='profile',parent=['profile-info-container','table-posts','privateKey-container', 'avatar-container'],data=containers_data,add = '1');
                                      }
                                      else {
                                          loadPage(page = 'profileForm', method = 'GET', prefix ='profile',parent=['profile-info-container','table-posts'],data=containers_data,add = '1');
                                      }
                                  })
                          }
                    });
}

var postReport = function(id) {
    var options = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                let response = fetch('/profile/post/report/'+id+'/',options)
                var options = {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                fetch('/profile/?id='+current_id,options)
                    .then((res) => res.json())
                    .then((json) => {
                        containers_data = [];
                        containers_data.push(profileInfo(json));
                        containers_data.push(postsInfo(json));
                        if (current_id == my_id) {
                            containers_data.push(privateKey_button);
                            containers_data.push(load_img_button);
                            loadPage(page = 'profileForm', method = 'GET', prefix ='profile',parent=['profile-info-container','table-posts','privateKey-container','avatar-container'],data=containers_data,add = '1');
                        }
                        else {
                            loadPage(page = 'profileForm', method = 'GET', prefix ='profile',parent=['profile-info-container','table-posts'],data=containers_data,add = '1');
                        }
                    })
}

var makeid = function(length) {
    var result           = [];
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result.push(characters.charAt(Math.floor(Math.random() *
 charactersLength)));
   }
   return result.join('');
}


var usersInfo = function(text) {
    table = '<p class="abc">Список пользователей:</p><table class="table"><tr><th>id</th><th>login</th><th>publicKey</th><th>addFriend</th></tr>';
    for (item of text) {

      table += '<tr><td>'+item.id+'</td> <td><a href="#" onclick="tryloadPage('+item.id+')">'

      if (item.login.length > 20){
        var modulo = Math.floor(item.login.length/20)
        for (let i=1; i<item.login.length/20;i++){
            table += '<p>'+item.login.slice((i-1)*20,20*i)+'</p>'
        }

        table += '<p>'+item.login.slice(20*modulo,)+'</p>'
      }
      else{
          table += item.login
      }

      table += '</a></td> <td>'

      if (item.publicKey.length > 60){
        var modulo = Math.floor(item.publicKey.length/60)
        for (let i=1; i<item.publicKey.length/60;i++){
            table += '<p>'+item.publicKey.slice((i-1)*60,60*i)+'</p>'
        }

        table += '<p>'+item.publicKey.slice(60*modulo,)+'</p>'
      }
      else{
          table += item.publicKey
      }

      logins_for_addFriend.set(item.id,item.login);
      table += '</td><td><a href="#" onclick="addFriend('+item.id+')">Добавить в друзья</a></td></tr>'
    }

    table += '</table>';
    return table;
}

var tryloadPage = function(id) {

    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    };
    fetch('/profile/?id='+id, options)
        .then((res) => res.json())
        .then((json) => {
            containers_data = [];
            current_id = id
            if(json.status == 'ok'){
                containers_data.push(profileInfo(json));
                containers_data.push(postsInfo(json));
                containers_data.push(privateKey_button);
                containers_data.push(load_img_button);
                loadPage(page = 'profileForm', method = 'GET', prefix ='profile',parent=['profile-info-container','table-posts','privateKey-container','avatar-container'],data=containers_data, add='1');
            }
            else {
                response = '<p>Вы пока что не друзья:(</p> <p>Но вы можете стать друзьями!</p>'

                loadPage(page='usersForm',method='GET',prefix='users',parent=['limit-container'],data=[users_table+response], add='1')
            }
        });
}

var logins_for_addFriend = new Map();

var addFriend = function(id) {
    var options = {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    };
                    fetch('/users/friends/add/?friendLogin='+logins_for_addFriend.get(id),options)
                        .then((res) => res.json())
                        .then((json) => {
                            if (json.status == 'ok') {
                                current_id = id;
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container'],data=[users_table+'<p></p>'+'<p>токен для получения челленджа: '+json.token+'</p>'],add='1');
                            }
                            else {
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container'],data=[users_table+'<p></p>'+'<p>Что-то пошло не так:(</p>'],add='1');
                            }
                        })
}

var loadPageUsers = function() {
    const options = {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                fetch('/users/?limit=8',options)
                    .then(function(response) {
                        if (response.ok) {
                            return response.json();
                        }
                        return Promise.reject(`Response status is ${response.status}, not available`);
                    })
                    .then((json) => {
                        containers_data = [];
                        users_table = usersInfo(json);
                        containers_data.push(users_table);
                        loadPage(page='usersForm',method='GET',prefix='users',parent=['limit-container'],data=containers_data,add='1')
                    });

}

var invites = function(limit) {
    var options = {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    };
                    fetch('/users/friends/tokens/?limit='+limit,options)
                        .then((res) => res.json())
                        .then((json) => {
                            if (json.length != 0) {
                                requests_table = requestsInfo(json)
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container'],data=[users_table+'<p></p>'+requests_table],add='1');
                            }
                            else {
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container'],data=[users_table+'Пока заявок не поступало'],add='1');
                            }
                        })
}

var requestsInfo = function(text) {
    table = '<p class="abc">Заявки в друзья:</p><table><tr><th>tokenId</th><th>initiator</th><th>person</th><th>token</th><th>challenge</th><th>secret</th><th>status</th></tr>';
    for (item of text) {
      table += '<tr><td>'+item.tokenId+'</td> <td>'

      if (item.initiator.length > 20){
        var modulo = Math.floor(item.initiator.length/20)
        for (let i=1; i<item.initiator.length/20;i++){
            table += '<p>'+item.initiator.slice((i-1)*20,20*i)+'</p>'
        }

        table += '<p>'+item.initiator.slice(20*modulo,)+'</p>'
      }
      else{
          table += item.initiator
      }

      table += '</td> <td>'

      if (item.person.length > 20){
        var modulo = Math.floor(item.person.length/20)
        for (let i=1; i<item.person.length/20;i++){
            table += '<p>'+item.person.slice((i-1)*20,20*i)+'</p>'
        }

        table += '<p>'+item.person.slice(20*modulo,)+'</p>'
      }
      else{
          table += item.person
      }

      table += '</td> <td>'+item.token

      if (item.status != 'DELETED'){
          table += '<p><a href="#" onclick="deleteToken('+item.tokenId+')">удалить</a></p>'
          tokens_for_invite.set(item.tokenId, item.token);
      }

      table += '</td><td>'+item.challenge+'</td><td>'+item.secret+'</td><td>'+item.status+'</td></tr>'
    }

    table += '</table>';
    return table;
}


var deleteToken = function(tokenId) {
    var options = {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    };
                    fetch('/users/friends/token/?token='+tokens_for_invite.get(tokenId),options)
                        .then((res) => res.json())
                        .then((json) => {
                            if (json.status == 'ok') {
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container'],data=[users_table+'<p>Токен успешно удален!</p>'],add='1');
                            }
                            else {
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container'],data=[users_table+'<p>Что-то пошло не так:(</p>'],add='1');
                            }
                        })
}


var loadMyProfile = function() {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    };
    fetch('/profile/?id='+my_id, options)
        .then((res) => res.json())
        .then((json) => {
            current_id = my_id;
            containers_data = [];
            containers_data.push(profileInfo(json));
            containers_data.push(postsInfo(json));
            containers_data.push(privateKey_button);
            containers_data.push(load_img_button);
            loadPage(page = 'profileForm', method = 'GET', prefix ='profile',parent=['profile-info-container','table-posts','privateKey-container','avatar-container'],data=containers_data, add='1');
        });
}

/**
 * Patch fetch to include additional 'Authorization' header
 * @param  {String} url      Original URL
 * @param  {Object} options  Original options
 * @return {Promise}
 */
var fetch = function(url, options = {}) {
    accessToken = tokenStorage.getToken();
    if (accessToken !== null) {
        if (options['headers'] !== undefined) {
            options['headers']['Authorization'] = `Bearer ${accessToken}`;
        } else {
            options['headers'] = {
                'Authorization': `Bearer ${accessToken}`
            }
        }
    }
    //alert(1)
    return defaultFetch(url, options);
}

/**
 * Get data from API ('/page'), update and navigate to '/#page'
 * @param  {String} page     page URL
 * @return {null}
 */
var loadPage = function(page = '', method = 'GET', prefix = '', parent = [], data = [],add = '') {
    const options = {
        method: method,
    };
    if (prefix !== '') {
        prefix = prefix + '/'
    }
    fetch('/' + prefix + page + '/', options)
        .then(function(response) {
            if (response.ok) {
                return response.text()
            }
            return Promise.reject(`Response status is ${response.status}, not available`);
        })
        .then((text) => {
            document.body.innerHTML = text;
            if (parent.length != 0) {
                for (let i=0;i<data.length;i++){
                    showStatus(parent[i],data[i]);
                }
            }

            window.location.hash = '#' + page + add;
            if (page == 'profileForm' && avatar.length > 0) {

                let img = document.querySelector('.image');
                img.src = 'data:image/png;base64,'+avatar;

            }
        });
}


/**
 * Add self-made enter listener (because we don't use "submit" listener)
 * @param  {String} element      element to click on
 * @param  {String} container    container to check events
 * @return {null}
 */
var enterListener = function(element, container) {
    window.document.getElementById(container).addEventListener('keyup', function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById(element).click();
        }
    });
}

/**
 * Add status from API, add to the parent block
 * @param  {String} parent     parent HTML element to append
 * @param  {String} message    status message
 * @return {null}
 */
var showStatus = function(parent, message = 'undefined') {
    const container = document.getElementById(parent);
    const element = document.createElement('div');
    element.innerHTML = message;
    container.appendChild(element);
}

window.addEventListener('hashchange', function(event) {
    switch (window.location.hash) {
      case '#auth':
          loadPage('auth');
      case '#registerForm':
            // Handle registration, send REST API backend request
            enterListener('register-button', 'register-container');
            window.document.getElementById('register-button').addEventListener('click', function() {
                const reg_login = document.getElementById('login1').value;
                const public_key = document.getElementById('public_key').value;
                const options = {
                    method: 'POST',
                    body: JSON.stringify({
                        'login': reg_login,
                        'public_key': public_key,
                    }),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                fetch('/auth/registration/', options)
                    .then((res) => res.json())
                    .then((json) => {
                        containers_data = []
                        if (json.status === 'ok') {
                            containers_data.push('Пользователь '+reg_login+' успешно зарегистрирован')
                            loadPage(page = 'registerForm', method = 'GET', prefix = 'auth',parent = ['your-id'], data = containers_data,add = '1')
                        }
                        else {
                            containers_data.push(json.message)
                            loadPage(page = 'registerForm', method = 'GET', prefix = 'auth',parent = ['your-id'], data = containers_data,add = '1')
                        }
                    });
            });
            break;
      case '#registerForm1':
            window.location.hash = '#registerForm'
            break;

      case '#loginForm':
            // Handle login, send REST API backend request
            enterListener('login-button', 'login-container');
            document.getElementById('login-button').addEventListener('click', function() {
                const login = document.getElementById('login2').value;
                const signed_login = document.getElementById('signed_login').value;
                const options = {
                    method: 'POST',
                    body: JSON.stringify({
                        'login': login,
                        'signed_login': signed_login,
                    }),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                fetch('/auth/login/', options)
                    .then((res) => res.json())
                    .then((json) => {
                        if (json.status === 'ok') {
                            tokenStorage.setToken(json.message);
                            my_id = JSON.parse(atob(json.message.split('.')[1]))["user_id"];
                            current_id = my_id;
                            loadMyProfile();
                        }
                        else {
                            loadPage(page = 'loginForm', method = 'GET', prefix = 'auth',parent = ['login-container'], data = [json.message] = '',add='1');
                        }
                    });
            });
            break;
      case '#loginForm1':
            window.location.hash = '#loginForm';
            break;
      case '#profileForm':
        const foo = document.getElementById('avatar-form')
        foo.addEventListener('submit', (e) => {
          e.preventDefault()
          const formData = new FormData(foo)
          fetch('/profile/upload/avatar', {
            method: 'POST',
            body: formData
          })
          loadMyProfile();
        })

        enterListener('privateKey-button', 'privateKey-container');
        document.getElementById('privateKey-button').addEventListener('click', function() {
                const privateKey = document.getElementById('privateKey').value;
                var options = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        'privateKey' : privateKey
                    }),
                };
                fetch('/profile/autoReplay/',options)
                    .then((res) => res.json())
                    .then((json) => {
                          if (json.status === 'ok') {
                              loadMyProfile();
                          }
                          else {
                              showStatus('privateKey-container',json.message);
                          }
                    });
        });

        enterListener('post-button', 'post-forms-container');
        document.getElementById('post-button').addEventListener('click', function() {
                const theme = document.getElementById('theme').value;
                const text = document.getElementById('text').value;
                const private_ = document.getElementById('is_private');
                var is_private = false;
                if (private_.checked){
                  is_private = true
                }
                const options = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        'theme': theme,
                        'text': text,
                        'is_private': is_private
                    }),
                };
                fetch('/profile/post/',options)
                    .then((res) => res.json())
                    .then((json) => {
                          if (json.status === 'ok') {
                              if (current_id == my_id) {
                                  loadMyProfile();
                              }
                              else {
                                  const options = {
                                      method: 'GET',
                                      headers: {
                                      'Content-Type': 'application/json',
                                      },
                                  };
                                  fetch('/profile/?id='+current_id,options)
                                      .then((res) => res.json())
                                      .then((json) => {
                                          containers_data = [];
                                          containers_data.push(profileInfo(json));
                                          containers_data.push(postsInfo(json));
                                          if (current_id == my_id) {
                                              containers_data.push(privateKey_button);
                                              containers_data.push(load_img_button);
                                              loadPage(page = 'profileForm', method = 'GET', prefix ='profile',parent=['profile-info-container','table-posts','privateKey-container','avatar-container'],data=containers_data,add = '1');
                                          }
                                          else {
                                              loadPage(page = 'profileForm', method = 'GET', prefix ='profile',parent=['profile-info-container','table-posts'],data=containers_data,add = '1');
                                          }
                                      })
                              }
                          }
                          else {
                              showStatus('post-forms-container', json.message);
                          }
                    });
            });

        enterListener('read-post-button', 'read-post-container');
        document.getElementById('read-post-button').addEventListener('click', function() {
                const token = document.getElementById('token').value;
                const signature = document.getElementById('signature').value;
                var options = {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                fetch('/profile/post/private/read/?token='+token+'&signature='+signature,options)
                    .then((res) => res.json())
                    .then((json) => {
                          if (json.status === 'ok') {
                              const theme = json.theme;
                              const text = json.text;
                              var options = {
                                  method: 'GET',
                                  headers: {
                                      'Content-Type': 'application/json',
                                  },
                              };
                              fetch('/profile/?id='+current_id,options)
                                  .then((res) => res.json())
                                  .then((json) => {
                                      containers_data = [];
                                      containers_data.push(profileInfo(json));
                                      containers_data.push(postsInfo(json));
                                      containers_data.push('<p>theme: '+theme+'</p><p>text: '+text+'</p>')
                                      if (current_id == my_id) {
                                          containers_data.push(privateKey_button);
                                          containers_data.push(load_img_button);
                                          loadPage(page = 'profileForm', method = 'GET', prefix ='profile',parent=['profile-info-container','table-posts','read-post-container','privateKey-container','avatar-container'],data=containers_data ,add = '1');
                                      }
                                      else {
                                          loadPage(page = 'profileForm', method = 'GET', prefix ='profile',parent=['profile-info-container','table-posts','read-post-container'],data=containers_data,add = '1');
                                      }
                                  })
                          }
                          else {
                              showStatus('read-post-container','Что-то пошло не так:(');
                          }
                    });
        });

        break;
      case '#profileForm1':
        window.location.hash = '#profileForm'
        break;
      case '#usersForm':
            enterListener('get-challenge-button', 'get-challenge-container');
            document.getElementById('get-challenge-button').addEventListener('click', function() {
                    const token_for_challenge = document.getElementById('token_for_challenge').value;
                    var options = {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    };
                    fetch('/users/challenge/?token='+token_for_challenge,options)
                        .then((res) => res.json())
                        .then((json) => {
                            if (json.status == 'ok' && json.challenge.length) {
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container','get-challenge-container'],data=[users_table,'challenge: '+json.challenge],add='1');
                            }
                            else {
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container','get-challenge-container'],data=[users_table,'Ваша заявка ещё не рассматривалась'],add='1');
                            }
                        })
              });

            enterListener('put-challenge-button', 'put-challenge-container');
            document.getElementById('put-challenge-button').addEventListener('click', function() {
                    const challenge_token = document.getElementById('challenge_token').value;
                    const put_challenge = document.getElementById('put_challenge').value;
                    const put_secret = document.getElementById('put_secret').value;
                    var options = {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                        'challenge': put_challenge,
                        'secret': put_secret,
                    }),
                    };
                    fetch('/users/challenge/?token='+challenge_token,options)
                        .then((res) => res.json())
                        .then((json) => {
                            if (json.status == 'ok') {
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container','put-challenge-container'],data=[users_table,'Челлендж успешно добавлен!'],add='1');
                            }
                            else {
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container','put-challenge-container'],data=[users_table,'Что-то пошло не так:('],add='1');
                            }
                        })
              });

            enterListener('challenge-solve-button', 'challenge-solve-container');
            document.getElementById('challenge-solve-button').addEventListener('click', function() {
                    const challenge_solve_token = document.getElementById('challenge_solve_token').value;
                    const challenge_answer = document.getElementById('challenge_answer').value;
                    var options = {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                        'token': challenge_solve_token,
                        'challenge_answer': challenge_answer,
                        'person': null
                    }),
                    };
                    fetch('/users/challenge/solve/',options)
                        .then((res) => res.json())
                        .then((json) => {
                            if (json.status == 'ok') {
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container','challenge-solve-container'],data=[users_table,'Поздравляем с приобритением нового друга!'],add='1');
                            }
                            else {
                                loadPage(page = 'usersForm', method = 'GET', prefix ='users',parent=['limit-container','challenge-solve-container'],data=[users_table,'Что-то пошло не так:('],add='1');
                            }
                        })
              });

        break;
      case '#usersForm1':
        window.location.hash = '#usersForm'
        break;
      case '#logout':
        tokenStorage.setToken(null);
        window.location = '#auth';
    }
});
