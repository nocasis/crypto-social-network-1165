<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
  <head>
    <link th:href="@{/css/users.css}" rel="stylesheet" />
    <link rel="icon" href="data:;base64,=">
    <script type="text/javascript" th:src="@{/js/social_network.js}"></script>
    <title>Пользователи</title>
  </head>
  <body>


<!---------------------Мой профиль и Выход------------------------------------------------------------->
      <a href="#" onclick="loadMyProfile();" id="my-profile-link">Мой профиль</a>
      <a href="#" onclick="loadPage(page = 'auth', method = 'GET', prefix = '');" id="logout-link">Выйти</a>
      <a href="#" onclick="invites(15);" id="friend-tokens-button">Заявки в друзья</a>
      <div class="user-data">

<!------------------Вывод пользователей--------------------------------------------->
          <div class="container" id="limit-container">

          </div>

          <div class="friends">
<!---------------------Решить челлендж---------------------------------------------->
              <div class="container" id="challenge-solve-container">
                  <p>Отправьте решение челленджа</p>
                  <p><input type="text" placeholder="Токен" id="challenge_solve_token"></p>
                  <p><input type="text" placeholder="Решение" id="challenge_answer"></p>
                  <input id="challenge-solve-button" class="dark" type="submit" value="Решить">
              </div>

<!---------------------Добавить челлендж-------------------------------------------->
              <div class="container" id="put-challenge-container">
                  <p>Добавьте челлендж для данного токена</p>
                  <p><input type="text" placeholder="Токен" id="challenge_token"></p>
                  <p><input type="text" placeholder="Челлендж" id="put_challenge"></p>
                  <p><input type="text" placeholder="Секрет" id="put_secret"></p>
                  <input id="put-challenge-button" class="dark" type="submit" value="Добавить">
              </div>

          </div>
          <div class="tmp">
<!---------------------Посмотреть заявки в друзья-----------------------------------
              <div class="container" id="friends-tokens-container">

              </div>

---------------------Получить челлендж по токену---------------------------------->
              <div class="container" id="get-challenge-container">
                  <p>Введите токен для получения челленджа</p>
                  <p><input type="text" placeholder="Токен" id="token_for_challenge"></p>
                  <input id="get-challenge-button" class="dark" type="submit" value="Получить">
              </div>







          </div>
      </div>
  </body>
</html>
