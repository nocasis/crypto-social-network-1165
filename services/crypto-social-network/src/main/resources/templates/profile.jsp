<!DOCTYPE html>
<html>
  <head>
      <meta charset="UTF-8">
      <link th:href="@{/css/profile.css}" rel="stylesheet" />
      <link rel="icon" href="data:;base64,=">
      <script type="text/javascript" th:src="@{/js/social_network.js}"></script>
      <title>Профиль</title>
  </head>
  <body>

<!---------------------Мой профиль и Выход------------------------------------------------------------->
      <a href="#" onclick="loadMyProfile();" id="my-profile-link">Мой профиль</a>
      <a href="#" onclick="loadPage(page = 'auth', method = 'GET', prefix = '');" id="logout-link">Выйти</a>
      <a href="#" onclick="loadPageUsers();" id="users-link">Друзья</a>

      <div class="bio">
<!---------------------Аватарка--------- -------------------------------------------------------------->
          <div class="avatarblock">
              <div class="container" id="image-container">
                  <img class="image" th:src="@{/img/2.jpg}">
              </div>
<!---------------------Загрузка аватара---------------------------------------------------------------->
          <div class="container" id="avatar-container">
          </div>
          </div>
<!---------------------Личные данные------------------------------------------------------------------->
          <div class="container" id="profile-info-container">
          </div>

<!---------------------Форма для поста-- -------------------------------------------------------------->
          <div class="container" id="post-forms-container">
            <p><input type="text" placeholder="Тема" id="theme"></p>
            <p><input class="text-content" type="text" placeholder="Содержание" id="text"></p>
            <p><input id="is_private" type="checkbox"> Сделать пост приватным?</p>
            <p><input id="post-button" class="dark" type="submit" value="Опубликовать"></p>
          </div>

<!---------------------Задание приватного ключа-------------------------------------------------------->
          <div class="container" id="privateKey-container">
          </div>

<!---------------------Таблица постов------------------------------------------------------------------>
          <div class="container" id="table-posts">
          </div>

<!---------------------Получение приватного поста------------------------------------------------------>
          <div class="container" id="read-post-container">
              <p class="text-privat">Получить приватный пост</p>
              <p><input type="text" placeholder="Введите токен" id="token"></p>
              <p><input type="text" placeholder="Введите подпись" id="signature"></p>
              <p><input id="read-post-button" class="dark" type="submit" value="Получить"></p>
          </div>
      </div>

  </body>
</html>
