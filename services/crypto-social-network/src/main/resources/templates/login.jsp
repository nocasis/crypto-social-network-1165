<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
  <head>
      <link th:href="@{/css/log.css}" rel="stylesheet" />
      <link rel="icon" href="data:;base64,=">
      <script type="text/javascript" th:src="@{/js/social_network.js}"></script>
      <title>Вход</title>
  </head>
  <body>
        <div class="mlog">
          <div class="container" id="login-container">
              <p class="reklama1">Здесь могла быть</p>
              <p class="reklama2">Ваша реклама!</p>
              <p class="reklama3">И здесь тоже!</p>
              <p><input class="login2" type="text" placeholder="Введите логин" id="login2"></p>
              <p><input class="key2" type="password" placeholder="Подпишите логин" id="signed_login"></p>
              <p><input id="login-button" class="dark" type="submit" value="Вход"></p>
              <a class="fail2" th:href="@{/img/bad.jpg}">Забыли ключ?</a>
          </div>
      </div>
  </body>
</html>
