<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
  <head>
      <link th:href="@{/css/auth.css}" rel="stylesheet" />
      <link rel="icon" href="data:;base64,=">
      <script type="text/javascript" th:src="@{/js/social_network.js}"></script>
      <title>#вкомбезе</title>
  </head>
  <body>
      <div class="container" id="index-container">
          <p>
              <a class="registr" href="#" onclick="loadPage(page = 'registerForm', method = 'GET', prefix = 'auth');" id="register-link">Регистрация</a>
              <a class="login" href="#" onclick="loadPage(page = 'loginForm', method = 'GET', prefix = 'auth');" id="login-link">Войти</a>
          </p>
      </div>
  </body>
</html>
