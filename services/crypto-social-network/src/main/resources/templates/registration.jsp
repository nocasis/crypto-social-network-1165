<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
  <head>
      <link th:href="@{/css/reg.css}" rel="stylesheet" />
      <link rel="icon" href="data:;base64,=">
      <script type="text/javascript" th:src="@{/js/social_network.js}"></script>
      <title>Регистрация</title>
  </head>
  <body>
      <div class="mreg">
          <div class="container" id="register-container">
              <div class="textreg">
                  <p class="hello">Пожалуйста, укажите Ваше имя и публичный ключ.</p>
                  <p>Чтобы облегчить общение и поиск флагов, у нас принято использовать</p>
                  <p>настоящие имена. Хотя, кого я обманываю, вводите уже свой</p>
                  <p>беспорядочный набор символов. Даже если что-то случайно упадет</p>
                  <p>на клавиатуру, мы это примем без вопросов. Главное не повторяемся!</p>
              </div>
              <p><input class="login1" type="text" placeholder="Введите логин" id="login1"></p>
              <p><input class="key" type="text" placeholder="Введите публичный ключ" id="public_key"></p>
              <p><input id="register-button" class="dark" type="submit" value="Регистрация"></p>

              <a class="log" href="#" onclick="loadPage('loginForm','GET','auth');" id="login-link">Войти</a>
              <a class="fail" th:href="@{/img/norm.jpg}">Не получается придумать?</a>
              <div class="yourid" id="your-id">
              </div>
          </div>
      </div>
  </body>
</html>
