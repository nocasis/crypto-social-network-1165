package ru.tsu.study.cryptosocialnetwork.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tsu.study.cryptosocialnetwork.model.Token;
import ru.tsu.study.cryptosocialnetwork.model.User;
import ru.tsu.study.cryptosocialnetwork.request.ChallengeRequest;
import ru.tsu.study.cryptosocialnetwork.request.SolveChallengeRequest;
import ru.tsu.study.cryptosocialnetwork.response.AddFriendResponse;
import ru.tsu.study.cryptosocialnetwork.response.ChallengeResponse;
import ru.tsu.study.cryptosocialnetwork.response.ProfileResponse;
import ru.tsu.study.cryptosocialnetwork.response.SolveChallengeResponse;
import ru.tsu.study.cryptosocialnetwork.service.UsersService;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @RequestMapping("/usersForm")
    public ModelAndView getUsersList() {
        return new ModelAndView("users");
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<User> getUsers(@RequestParam Long limit) {
        return usersService.findAllUsers(limit);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody User getUserById(@PathVariable Long id) {
        return usersService.findUserById(id);
    }

    @PostMapping(path = "/friends/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AddFriendResponse> addToFriends(@RequestParam String friendLogin, @RequestHeader(name = "Authorization") String header) {
        return usersService.addFriend(friendLogin, header);
    }

    @GetMapping(path = "/friends/tokens", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Token> getTokens(@RequestParam Long limit, @RequestHeader(name = "Authorization") String header) {
        return usersService.findAllTokensByPerson(header, limit);
    }

    @DeleteMapping(path = "/friends/token", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileResponse> removeToken(@RequestParam String token) {
        return usersService.removeToken(token);
    }

    @PutMapping(path = "/challenge", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AddFriendResponse> putChallenge(@RequestParam String token, @RequestBody ChallengeRequest challengeRequest) {
        return usersService.putChallenge(token, challengeRequest);
    }

    @GetMapping(path = "/challenge", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChallengeResponse> getChallenge(@RequestParam String token) {
        return usersService.getChallenge(token);
    }

    @PutMapping(path = "/challenge/solve", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SolveChallengeResponse>  solveChallenge(@RequestBody SolveChallengeRequest solveChallengeRequest, @RequestHeader(name = "Authorization") String header) {
        return usersService.solveChallenge(solveChallengeRequest, header);
    }
}
