package ru.tsu.study.cryptosocialnetwork.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.study.cryptosocialnetwork.request.AutoReplayRequest;
import ru.tsu.study.cryptosocialnetwork.request.CreatePostRequest;
import ru.tsu.study.cryptosocialnetwork.response.*;
import ru.tsu.study.cryptosocialnetwork.service.ProfileService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;


@Controller
@RequestMapping("/profile")
public class ProfileController {

    final static Logger LOGGER = LogManager.getLogger(ProfileController.class);

    private final ProfileService profileService;

    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }


    @GetMapping("/profileForm")
    public ModelAndView getProfile() {
        return new ModelAndView("profile");
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> getProfile(@RequestParam long id, @RequestHeader(name = "Authorization") String header) {
        return profileService.getProfile(id, header);
    }


    @PostMapping(path = "/post", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CreatePostResponse> createPost(@RequestBody CreatePostRequest createPostRequest, @RequestHeader(name = "Authorization") String header) {
        LOGGER.info(createPostRequest);
        return profileService.createPost(createPostRequest, header);
    }

    @PostMapping(path = "/upload/avatar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileResponse> uploadAvatar(@RequestParam("file") MultipartFile file, @RequestHeader(name = "Authorization") String header) {
        return profileService.uploadAvatar(file, header);
    }

    @GetMapping(path = "/avatar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AvatarResponse> getAvatar(@RequestParam long id, @RequestHeader(name = "Authorization") String header) {
        return profileService.getAvatar(id, header);
    }

    @PostMapping(path = "/post/report/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileResponse> reportPost(@PathVariable long id) {
        return profileService.reportPost(id);
    }

    @GetMapping(path = "/post/private", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PrivatePostChallengeResponse> getPrivatePostChallenge(@RequestParam long id, @RequestHeader(name = "Authorization") String header) {
        return profileService.getPrivatePostChallenge(id, header);
    }

    @GetMapping(path = "/post/private/read", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PrivatePostResponse> readPrivatePost(@RequestParam String token, @RequestParam String signature, @RequestHeader(name = "Authorization") String header) {
        return profileService.readPrivatePost(token, signature, header);
    }

    @PostMapping(path = "/autoReplay", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileResponse> setAutoReplay(@RequestBody AutoReplayRequest autoReplayRequest, @RequestHeader(name = "Authorization") String header) {
        LOGGER.info(autoReplayRequest);
        return profileService.setAutoReplay(autoReplayRequest, header);
    }
}
