package ru.tsu.study.cryptosocialnetwork.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.tsu.study.cryptosocialnetwork.model.Token;
import ru.tsu.study.cryptosocialnetwork.model.User;
import ru.tsu.study.cryptosocialnetwork.repository.TokenRepository;
import ru.tsu.study.cryptosocialnetwork.repository.UserRepository;
import ru.tsu.study.cryptosocialnetwork.request.ChallengeRequest;
import ru.tsu.study.cryptosocialnetwork.request.SolveChallengeRequest;
import ru.tsu.study.cryptosocialnetwork.response.*;
import ru.tsu.study.cryptosocialnetwork.security.Crypto;
import ru.tsu.study.cryptosocialnetwork.security.JwtFilter;
import ru.tsu.study.cryptosocialnetwork.security.JwtProvider;

import java.util.List;

@Component("usersService")
public class UsersService {
    private static final Logger LOGGER = LogManager.getLogger(UsersService.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private JwtProvider jwtProvider;

    public UsersService(){}

    public List<User> findAllUsers(Long limit) {
        if (limit > 100L) {
            limit = 100L;
        }
        return userRepository.findAllUsers(limit);
    }

    public User findUserById(Long id) {
        User user = userRepository.findById(id).get();
        user.setPrivateKey("");
        user.setFriendsIds("");
        return user;
    }


    public ResponseEntity<AddFriendResponse> addFriend(String friendLogin, String header) {
        try{
            User friend = userRepository.findByLogin(friendLogin);
            String initiatorsToken = JwtFilter.getTokenFromHeader(header);
            String initiatorsLogin = jwtProvider.getLoginFromToken(initiatorsToken);
            if(friendLogin.equals(initiatorsLogin)) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new AddFriendResponse("error", "it is you", ""));
            }
            if(!friend.getPrivateKey().isEmpty()) {
                User initiator = userRepository.findByLogin(initiatorsLogin);
                return autoAddFriend(initiator, friend);
            }

            Token token = new Token(initiatorsLogin, friendLogin, Crypto.generateUUID());
            tokenRepository.save(token);
            return ResponseEntity.ok().body(new AddFriendResponse("ok", "", token.getToken()));
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new AddFriendResponse("error", "friend id doesn't exist", ""));
        } catch (Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new AddFriendResponse("error", "unhandled error occurred", ""));
        }
    }


    public ResponseEntity<AddFriendResponse> autoAddFriend(User initiator, User friend) {
        try{
            byte[] sharedSecret = Crypto.doKeyAgree(friend.getPrivateKey(), initiator.getPublicKey());
            String secret = Crypto.getRandomSecret();

            String challenge = Crypto.aes(secret, sharedSecret);

            Token token = new Token(initiator.getLogin(), friend.getLogin(), Crypto.generateUUID());
            token.setChallenge(challenge);
            token.setSecret(secret);
            token.setStatus(Token.Status.ACTIVE);
            tokenRepository.save(token);

            return ResponseEntity.ok().body(new AddFriendResponse("ok", "", token.getToken()));
        } catch (Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new AddFriendResponse("error", "unhandled error occurred", ""));
        }
    }


    public List<Token> findAllTokensByPerson(String header, Long limit) {
        if (limit > 100L) {
            limit = 100L;
        }
        String initiatorsToken = JwtFilter.getTokenFromHeader(header);
        String initiatorsLogin = jwtProvider.getLoginFromToken(initiatorsToken);
        return tokenRepository.findAllTokensByPerson(initiatorsLogin, limit);
    }

    public ResponseEntity<AddFriendResponse> putChallenge(String tokenValue, ChallengeRequest request) {
        try{
            LOGGER.info(request);
            String challenge = request.getChallenge();
            String secret = request.getSecret();

            if (challenge.isEmpty() || secret.isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new AddFriendResponse("error", "challenge or secret is empty", ""));
            }

            if (secret.length() > 24) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new AddFriendResponse("error", "secret is too long", ""));
            }

            Token token = tokenRepository.findTokenByTokenValue(tokenValue);

            if(!token.getStatus().equals(Token.Status.NEW)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new AddFriendResponse("error", "token is not new", ""));
            }

            token.setChallenge(challenge);
            token.setSecret(secret);
            token.setStatus(Token.Status.ACTIVE);
            tokenRepository.save(token);
            return ResponseEntity.ok().body(new AddFriendResponse("ok", "", token.getToken()));
        } catch (EmptyResultDataAccessException e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new AddFriendResponse("error", "token doesn't exist", ""));
        } catch (Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new AddFriendResponse("error", "unhandled error occurred", ""));
        }
    }

    public ResponseEntity<ChallengeResponse> getChallenge(String tokenValue) {
        try{

            Token token = tokenRepository.findTokenByTokenValue(tokenValue);
            if(!token.getStatus().equals(Token.Status.ACTIVE)) {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new ChallengeResponse("error", "not active token", ""));
            }
            token.setSecret("");
            return ResponseEntity.ok().body(new ChallengeResponse("ok", "", token.getChallenge()));
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ChallengeResponse("error", "token doesn't exist", ""));
        } catch (Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ChallengeResponse("error", "unhandled error occurred", ""));
        }
    }

    public ResponseEntity<SolveChallengeResponse> solveChallenge(SolveChallengeRequest request, String header) {
        try{
            LOGGER.info(request);

            Token token = tokenRepository.findTokenByTokenValue(request.getToken());

            if (!token.getSecret().equals(request.getChallengeAnswer())) {
                LOGGER.warn(token.getSecret() + " != " + request.getChallengeAnswer());
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new SolveChallengeResponse("error", "wrong answer",""));
            }
            token.setStatus(Token.Status.SOLVED);
            tokenRepository.save(token);

            String initiatorsToken = JwtFilter.getTokenFromHeader(header);
            Long initiatorId = Long.parseLong(jwtProvider.getUserIdFromToken(initiatorsToken));
            User initiator = userRepository.findById(initiatorId).get();
            User user =  !request.getPerson().isEmpty() || request.getPerson() != null ? userRepository.findByLogin(request.getPerson()) : userRepository.findByLogin(token.getPerson());
            user.addToFriend(initiatorId);
            initiator.addToFriend(user.getId());
            userRepository.save(initiator);
            userRepository.save(user);

            return ResponseEntity.ok().body(new SolveChallengeResponse("ok", "Now you are friends with ", request.getPerson()));
        } catch (EmptyResultDataAccessException e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new SolveChallengeResponse("error", "token doesn't exist", ""));
        } catch (Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new SolveChallengeResponse("error", "unhandled error occurred", ""));
        }
    }

    public ResponseEntity<ProfileResponse> removeToken(String tokenValue) {
        try{

            Token token = tokenRepository.findTokenByTokenValue(tokenValue);
            token.setStatus(Token.Status.DELETED);
            tokenRepository.save(token);
            return ResponseEntity.ok().body(new ProfileResponse("ok", "token removed" ));
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ProfileResponse("error", "token doesn't exist"));
        } catch (Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ProfileResponse("error", "unhandled error occurred"));
        }
    }

}
