package ru.tsu.study.cryptosocialnetwork.request;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

public class CreatePostRequest {
    @JsonProperty("theme")
    private String theme;
    @JsonProperty("text")
    private String text;
    @JsonProperty("is_private")
    private boolean isPrivate;

    @JsonGetter("theme")
    public String getTheme() {
        return theme;
    }

    @JsonSetter("theme")
    public void setTheme(String theme) {
        this.theme = theme;
    }

    @JsonGetter("text")
    public String getText() {
        return text;
    }

    @JsonSetter("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonGetter("is_private")
    public boolean getIsPrivate() {
        return isPrivate;
    }

    @JsonSetter("is_private")
    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String toString() {
        return "CreatePostRequest = {theme: " + theme + ", text=" + text + "}";
    }
}
