package ru.tsu.study.cryptosocialnetwork.repository;

import org.springframework.data.repository.CrudRepository;
import ru.tsu.study.cryptosocialnetwork.model.Token;

public interface TokenRepository extends CrudRepository<Token, Long>, TokenRepositoryCustom {
}
