package ru.tsu.study.cryptosocialnetwork.model;


import javax.persistence.*;


@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "login")
    private String login;
    @Column(name = "public_key")
    private String publicKey;
    @Column(name = "private_key", length=300)
    private String privateKey;
    @Column(name = "friends_ids", length=10240)
    private String friendsIds;
    @Column(name = "avatar")
    private String avatar;

    public User() {
    }

    public User(String login, String publicKey) {
        this.login = login;
        this.publicKey = publicKey;
        this.privateKey = "";
        this.friendsIds = "";
        this.avatar = "";
    }

    public User(Long id, String login, String publicKey) {
        this.id = id;
        this.login = login;
        this.publicKey = publicKey;
        this.privateKey = "";
        this.friendsIds = "";
        this.avatar = "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getFriendsIds() {
        return friendsIds;
    }

    public void setFriendsIds(String friendsIds) {
        this.friendsIds = friendsIds;
    }

    public void addToFriend(Long id) {
        this.friendsIds += id.toString() + ",";
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
