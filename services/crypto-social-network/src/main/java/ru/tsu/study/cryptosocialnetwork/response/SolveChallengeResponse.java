package ru.tsu.study.cryptosocialnetwork.response;

public class SolveChallengeResponse {
    private String status;
    private String message;
    private String person;

    public SolveChallengeResponse(String status, String message, String person) {
        this.status = status;
        this.message = message;
        this.person = person;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }
}
