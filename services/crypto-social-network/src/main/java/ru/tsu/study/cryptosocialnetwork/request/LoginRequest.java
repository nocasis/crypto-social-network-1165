package ru.tsu.study.cryptosocialnetwork.request;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;


public class LoginRequest {
    @JsonProperty("login")
    private String login;
    @JsonProperty("signed_login")
    private String signedLogin;

    @JsonGetter("login")
    public String getLogin() {
        return login;
    }

    @JsonSetter("login")
    public void setLogin(String login) {
        this.login = login;
    }

    @JsonGetter("signed_login")
    public String getSignedLogin() {
        return signedLogin;
    }

    @JsonSetter("signed_login")
    public void setSignedLogin(String signedLogin) {
        this.signedLogin = signedLogin;
    }

    public String toString() {
        return "LoginRequest = {login: " + login + ", signed_login: " + signedLogin + "}";
    }
}
