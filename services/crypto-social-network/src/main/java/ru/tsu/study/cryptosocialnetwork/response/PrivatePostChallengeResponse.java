package ru.tsu.study.cryptosocialnetwork.response;

public class PrivatePostChallengeResponse {
    private String status;
    private String message;
    private String token;
    private String secret;

    public PrivatePostChallengeResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public PrivatePostChallengeResponse(String status, String token, String secret) {
        this.status = status;
        this.token = token;
        this.secret = secret;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
