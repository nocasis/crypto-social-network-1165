package ru.tsu.study.cryptosocialnetwork.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tsu.study.cryptosocialnetwork.model.Post;
import ru.tsu.study.cryptosocialnetwork.service.AdminService;

import java.util.List;


@RestController
@RequestMapping("/admin")
public class AdminController {

    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping(path = "/log", produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String getLogs() {
        return adminService.readLog();
    }

    @GetMapping(path = "/post/reported", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Post getReportedPost(@RequestParam Long id) {
        return adminService.getReportedPost(id);
    }


    @GetMapping(path = "/all/post/reported", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Post> getAllReportedPost(@RequestParam Long limit) {
        return adminService.getAllReportedPost(limit);
    }

    @DeleteMapping(path = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteUserById(@RequestParam Long id) {
        return adminService.deleteUserById(id);
    }

    @DeleteMapping(path = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deletePost(@RequestParam Long id) {
        return adminService.deleteById(id);
    }

}
