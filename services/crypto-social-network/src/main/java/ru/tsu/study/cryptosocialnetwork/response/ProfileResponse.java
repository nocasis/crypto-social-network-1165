package ru.tsu.study.cryptosocialnetwork.response;

public class ProfileResponse {
    private String status;
    private String message;


    public ProfileResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public ProfileResponse(String status) {
        this.status = status;
        this.message = "";
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
