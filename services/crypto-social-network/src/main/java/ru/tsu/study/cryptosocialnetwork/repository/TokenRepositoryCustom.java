package ru.tsu.study.cryptosocialnetwork.repository;

import org.springframework.data.repository.NoRepositoryBean;
import ru.tsu.study.cryptosocialnetwork.model.Token;

import java.util.List;

@NoRepositoryBean
public interface TokenRepositoryCustom {
    List<Token> findAllTokensByPerson(String person, Long limit);
    Token findTokenByTokenValue(String token);
}
