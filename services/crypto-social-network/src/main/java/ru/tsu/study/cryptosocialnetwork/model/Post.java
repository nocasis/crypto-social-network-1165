package ru.tsu.study.cryptosocialnetwork.model;

import javax.persistence.*;
@Entity
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private Long postId;
    @Column(name = "owner_id")
    private Long ownerId;
    @Column(name = "theme")
    private String theme;
    @Column(name = "text", length=1024)
    private String text;
    @Column(name = "is_private")
    private boolean isPrivate;
    @Column(name = "reports_count")
    private Long reportsCount;

    public Post() {
    }

    public Post(Long ownerId, String theme, String text, boolean isPrivate, Long reportsCount) {
        this.ownerId = ownerId;
        this.theme = theme;
        this.text = text;
        this.isPrivate = isPrivate;
        this.reportsCount = reportsCount;
    }

    public Post(Long postId, Long ownerId, boolean isPrivate, String theme, String text, Long reportsCount) {
        this.postId = postId;
        this.ownerId = ownerId;
        this.isPrivate = isPrivate;
        this.theme = theme;
        this.text = text;
        this.reportsCount = reportsCount;
    }

    public Post(Long postId, Long ownerId, boolean isPrivate, Long reportsCount) {
        this.postId = postId;
        this.ownerId = ownerId;
        this.isPrivate = isPrivate;
        this.theme = "";
        this.text = "";
        this.reportsCount = reportsCount;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long id) {
        this.postId = id;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public Long getReportsCount() {
        return reportsCount;
    }

    public void setReportsCount(Long reportsCount) {
        this.reportsCount = reportsCount;
    }
}
