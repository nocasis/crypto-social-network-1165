package ru.tsu.study.cryptosocialnetwork.repository;

import org.springframework.data.repository.NoRepositoryBean;
import ru.tsu.study.cryptosocialnetwork.model.Post;

import java.util.List;

@NoRepositoryBean
public interface PostRepositoryCustom {

    List<Post> findAllUserPublicPosts(Long ownerId);
    List<Post> findAllUserPrivatePosts(Long ownerId);
    Post reportPost(Long id);
    Post getReportedPost(Long id);
    List<Post> getAllReportedPost(Long limit);
}
