package ru.tsu.study.cryptosocialnetwork.service;

import org.apache.commons.io.input.ReversedLinesFileReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.tsu.study.cryptosocialnetwork.model.Post;
import ru.tsu.study.cryptosocialnetwork.model.User;
import ru.tsu.study.cryptosocialnetwork.repository.PostRepository;
import ru.tsu.study.cryptosocialnetwork.repository.UserRepository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component("adminService")
public class AdminService {
    private static final Logger LOGGER = LogManager.getLogger(AdminService.class);

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UserRepository userRepository;

    @Value("${admin.logs.file}")
    public String logFile;

    public Post getReportedPost(Long id) {
        try{
            return postRepository.getReportedPost(id);
        } catch (Exception e) {
            LOGGER.error(e);
            return new Post();
        }
    }

    public List<Post> getAllReportedPost(Long limit) {
        try{
            if (limit > 100L) {
                limit = 100L;
            }
            return postRepository.getAllReportedPost(limit);
        } catch (Exception e) {
            LOGGER.error(e);
            return new ArrayList<Post>();
        }
    }

    public ResponseEntity<String> deleteById(Long id) {
        try {
            Post post = postRepository.getReportedPost(id);
            postRepository.deleteById(post.getPostId());
            return ResponseEntity.status(HttpStatus.OK).body("{\"message\": \"post deleted\"}");
        } catch (Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\": \"post doesn't exist\"}");
        }
    }


    public ResponseEntity<String> deleteUserById(Long id) {
        try {

            User user = userRepository.findById(id).get();

            deleteFriends(user);
            userRepository.deleteById(id);

            List<Post> posts = postRepository.findAllUserPublicPosts(id);
            List<Post> privatePosts = postRepository.findAllUserPrivatePosts(id);
            posts.addAll(privatePosts);

            for(Post post: posts) {
                postRepository.deleteById(post.getPostId());
            }

            return ResponseEntity.status(HttpStatus.OK).body("{\"message\": \"user deleted\"}");
        } catch (Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\": \"user doesn't exist\"}");
        }
    }

    public void deleteFriends(User user) {
        try {

            List<Long> ids = ServiceUtil.getIdsFromString(user.getFriendsIds());

            User friend;
            for (Long friendId : ids) {
                if (friendId.equals(user.getId())) {
                    continue;
                }
                friend = userRepository.findById(friendId).get();
                deleteFriend(friend, user.getId());
            }

        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    public void deleteFriend(User friend, Long id) {
        try{
            List<Long> ids = ServiceUtil.getIdsFromString(friend.getFriendsIds());
            StringBuilder sb = new StringBuilder();

            for (Long friendId : ids) {
                if (friendId.equals(id)) {
                    continue;
                }
                sb.append(friendId.toString()).append(",");
            }

            friend.setFriendsIds(sb.toString());
            userRepository.save(friend);
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }


    public String readLog() {
        try {
            int n_lines = 1000;
            ReversedLinesFileReader object = new ReversedLinesFileReader(new File(logFile));
            StringBuilder result = new StringBuilder();
            for(int i = 0;i < n_lines; i++){
                String line=object.readLine();
                if(line==null)
                    break;
                result.append(line).append("\n");
            }
            return result.toString();
        } catch(IOException e) {
            LOGGER.error(e);
            return "";
        }
    }
}
