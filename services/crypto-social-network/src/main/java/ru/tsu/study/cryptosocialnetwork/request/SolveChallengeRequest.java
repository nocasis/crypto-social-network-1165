package ru.tsu.study.cryptosocialnetwork.request;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

public class SolveChallengeRequest {
    @JsonProperty("token")
    private String token;
    @JsonProperty("challenge_answer")
    private String challengeAnswer;
    @JsonProperty("person")
    private String person;

    @JsonGetter("token")
    public String getToken() {
        return token;
    }

    @JsonGetter("token")
    public void setToken(String token) {
        this.token = token;
    }

    @JsonGetter("challenge_answer")
    public String getChallengeAnswer() {
        return challengeAnswer;
    }

    @JsonSetter("challenge_answer")
    public void setChallengeAnswer(String challengeAnswer) {
        this.challengeAnswer = challengeAnswer;
    }

    @JsonGetter("person")
    public String getPerson() {
        return person;
    }

    @JsonSetter("person")
    public void setPerson(String person) {
        this.person = person;
    }

}
