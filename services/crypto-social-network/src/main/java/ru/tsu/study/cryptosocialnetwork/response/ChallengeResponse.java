package ru.tsu.study.cryptosocialnetwork.response;

public class ChallengeResponse {
    private String status;
    private String message;
    private String challenge;



    public ChallengeResponse(String status, String message, String challenge) {
        this.status = status;
        this.message = message;
        this.challenge = challenge;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }
}
