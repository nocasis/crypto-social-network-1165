package ru.tsu.study.cryptosocialnetwork.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.tsu.study.cryptosocialnetwork.model.User;
import ru.tsu.study.cryptosocialnetwork.repository.UserRepository;
import ru.tsu.study.cryptosocialnetwork.security.CustomUserDetails;

@Component
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(username);
        return CustomUserDetails.fromUserEntityToCustomUserDetails(user);
    }

}
