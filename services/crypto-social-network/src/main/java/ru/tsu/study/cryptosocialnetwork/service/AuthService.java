package ru.tsu.study.cryptosocialnetwork.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Component;
import ru.tsu.study.cryptosocialnetwork.model.User;
import ru.tsu.study.cryptosocialnetwork.repository.UserRepository;
import ru.tsu.study.cryptosocialnetwork.request.AdminAuthRequest;
import ru.tsu.study.cryptosocialnetwork.request.LoginRequest;
import ru.tsu.study.cryptosocialnetwork.request.RegistrationRequest;
import ru.tsu.study.cryptosocialnetwork.response.AdminAuthResponse;
import ru.tsu.study.cryptosocialnetwork.response.AuthResponse;
import ru.tsu.study.cryptosocialnetwork.security.Crypto;
import ru.tsu.study.cryptosocialnetwork.security.JwtProvider;

import java.io.*;
import java.util.Arrays;
import java.util.stream.Stream;


@Component("authService")
public class AuthService {
    private static final Logger LOGGER = LogManager.getLogger(AuthService.class);

    @Value("${admin.access_tokens.dir}")
    public String adminAccessTokensDir;
    @Value("${admin.access_tokens.count}")
    public int maxTokensCount;
    @Value("${admin.secret.file.size}")
    public int secretFileSize;
    @Value("${admin.public_key}")
    private String adminPublicKey;
    public static String adminAccessTokensFile = "access_tokens.txt";
    public static String secretsFile = "secrets.txt";

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtProvider jwtProvider;

    public AuthService(){}

    public ResponseEntity<AuthResponse> register(RegistrationRequest request) {
        try{
            String login = request.getLogin();
            String publicKey = request.getPublicKey();

            if (login.isEmpty() || publicKey.isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new AuthResponse("error", "login or public_key is empty"));
            }

            if (userRepository.userExistsByLogin(login)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new AuthResponse("error", "user already exists"));
            }
            User user = new User(login, publicKey);
            User newUser = userRepository.save(user);
            return ResponseEntity.ok((new AuthResponse("ok", String.valueOf(newUser.getId()))));
        }
        catch (Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new AuthResponse("error", "unhandled error occurred"));
        }
    }

    public ResponseEntity<AuthResponse> login(LoginRequest request) {
        try{
            String login = request.getLogin();
            String signedLogin = request.getSignedLogin();

            if (login.isEmpty() || signedLogin.isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new AuthResponse("error", "login or signed_login is empty"));
            }

            User user = userRepository.findByLogin(login);

            if(!Crypto.verifySign(user.getPublicKey(), signedLogin, login)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new AuthResponse("error", "bad signature"));
            }

            String token = jwtProvider.generateToken(user.getId(), user.getLogin(), user.getPublicKey());

            return ResponseEntity.ok((new AuthResponse("ok", token)));
        }
        catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new AuthResponse("error", "wrong login or signature"));
        } catch(Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new AuthResponse("error", "unhandled error occurred"));
        }
    }

    public ResponseEntity<AdminAuthResponse> startAdminAuth() {
        try {
            String secret = Crypto.getRandomSecret();
            putNewSecret(secret);

            byte[] keyData = Crypto.doKeyAgreeWithAdmin(adminPublicKey);
            byte[] sharedKey = Arrays.copyOfRange(keyData, 0, 32);
            byte[] publicKey = Arrays.copyOfRange(keyData, 32, keyData.length);

            String encryptedData = Crypto.aes(secret, sharedKey);

            return ResponseEntity.ok((new AdminAuthResponse("ok", new String(Hex.encode(publicKey)) , encryptedData)));
        } catch(Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new AdminAuthResponse("error", "unhandled error occurred"));
        }
    }

    public ResponseEntity<AdminAuthResponse> finishAdminAuth(AdminAuthRequest request) {
        try {

            if (request.getSecret().isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new AdminAuthResponse("error", "secret is empty"));
            }

            String secret = request.getSecret();
            if (!checkLineInFile(secret, secretsFile)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new AdminAuthResponse("error", "secret doesn't exist"));
            }

            String token = Crypto.generateUUID();
            putNewAdminToken(token);

            return ResponseEntity.ok((new AdminAuthResponse("ok",  "Hello, admin!", token)));
        } catch(Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new AdminAuthResponse("error", "unhandled error occurred"));
        }
    }

    public void putNewSecret(String secret) {
        try {
            File tokensDir = new File(adminAccessTokensDir);
            if (!tokensDir.exists()) {
                tokensDir.mkdirs();
            }
            File secretFile = new File(tokensDir.getAbsolutePath() + "/" + secretsFile);
            if (!secretFile.exists()) {
                secretFile.createNewFile();
            }

            if (secretFile.length() >= secretFileSize) {
                secretFile.delete();
                secretFile.createNewFile();
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter(secretFile, true));
            writer.write(secret+"\n");
            writer.close();

        } catch(IOException e) {
            LOGGER.error(e);
        }
    }

    public void putNewAdminToken(String token) {
        try {
            File tokensDir = new File(adminAccessTokensDir);
            if (!tokensDir.exists()) {
                tokensDir.mkdirs();
            }
            File tokensFile = new File(tokensDir.getAbsolutePath() + "/" + adminAccessTokensFile);
            if (!tokensFile.exists()) {
                tokensFile.createNewFile();
            }

            BufferedReader reader = new BufferedReader(new FileReader(tokensFile));
            int linesCount = (int) reader.lines().count();
            if (linesCount >= maxTokensCount) {
                tokensFile.delete();
                tokensFile.createNewFile();
            }
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(tokensFile, true));
            writer.write(token+"\n");
            writer.close();

        } catch(IOException e) {
            LOGGER.error(e);
        }
    }


    public boolean checkLineInFile(String line, String filename) {
        try {
            File tokensDir = new File(adminAccessTokensDir);
            if (!tokensDir.exists()) {
                tokensDir.mkdirs();
            }
            File tokensFile = new File(tokensDir.getAbsolutePath() + "/" + filename);
            if (!tokensFile.exists()) {
                return false;
            }
            BufferedReader reader = new BufferedReader(new FileReader(tokensFile));
            Stream<String> lines = reader.lines();

            return lines.anyMatch(s -> s.startsWith(line));
        } catch(IOException e) {
            LOGGER.error(e);
            return false;
        }
    }

}
