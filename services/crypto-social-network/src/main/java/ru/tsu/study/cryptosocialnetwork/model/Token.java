package ru.tsu.study.cryptosocialnetwork.model;

import javax.persistence.*;
@Entity
@Table(name = "challenge_tokens")
public class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "token_id")
    private Long tokenId;
    @Column(name = "initiator")
    private String initiator;
    @Column(name = "person")
    private String person;
    @Column(name = "token")
    private String token;
    @Column(name = "challenge")
    private String challenge;
    @Column(name = "secret")
    private String secret;
    @Column(name = "status")
    private Status status;

    public Token() {
    }

    public enum Status{NEW, ACTIVE, SOLVED, DELETED}

    public Token(String initiator, String person, String token) {
        this.initiator = initiator;
        this.person = person;
        this.token = token;
        this.challenge = "";
        this.secret = "";
        this.status = Status.NEW;
    }

    public Token(Long tokenId, String initiator, String person, String token, String challenge, String secret, Status status) {
        this.tokenId = tokenId;
        this.initiator = initiator;
        this.person = person;
        this.token = token;
        this.challenge = challenge;
        this.secret = secret;
        this.status = status;
    }

    public Long getTokenId() {
        return tokenId;
    }

    public void setTokenId(Long tokenId) {
        this.tokenId = tokenId;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getChallenge() {
        return challenge;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String isChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


}
