package ru.tsu.study.cryptosocialnetwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CryptoSocialNetworkApplication {


    public static void main(String[] args) {
        SpringApplication.run(CryptoSocialNetworkApplication.class, args);
    }

}
