package ru.tsu.study.cryptosocialnetwork.repository;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import ru.tsu.study.cryptosocialnetwork.model.User;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class UserRepositoryImpl implements UserRepositoryCustom{

    private static final Logger LOGGER = LogManager.getLogger(UserRepositoryImpl.class);

    @PersistenceContext
    EntityManager em;

    @Override
    public User findByLogin(String login) throws NoResultException {

        String nativeQuery = "SELECT * FROM users WHERE login='" + login + "'";
        Query query = em.createNativeQuery(nativeQuery, User.class);

        return (User) query.getSingleResult();
    }

    @Override
    public boolean userExistsByLogin(String login) throws NoResultException {
        try{
            String nativeQuery = "SELECT * FROM users WHERE login='" + login + "'";
            LOGGER.info(nativeQuery);
            Query query = em.createNativeQuery(nativeQuery, User.class);
            query.getSingleResult();
            return true;

        } catch(NoResultException e) {
            return false;
        } catch(Exception e) {
            LOGGER.info(e);
            return false;
        }
    }

    @Override
    public List<User> findAllUsers(Long limit) throws NoResultException {
        String nativeQuery = "SELECT * FROM users ORDER BY id DESC LIMIT " + limit;
        Query query = em.createNativeQuery(nativeQuery, User.class);
        List<User> users = new ArrayList<User>();
        for(User user: (List<User>) query.getResultList()) {
            users.add(new User(user.getId(), user.getLogin(), user.getPublicKey()));
        }
        return users;
    }


}
