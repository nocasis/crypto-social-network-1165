package ru.tsu.study.cryptosocialnetwork.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import ru.tsu.study.cryptosocialnetwork.service.AuthService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class AdminFilter extends GenericFilterBean {

    private static final Logger LOGGER = LogManager.getLogger(AdminFilter.class);
    public static final String ADMIN_AUTHORIZATION = "Admin-Authorization";
    public static final String REMOTE_HOST = "X-Forwarded-Host";
    private static final String WHITE_IP = "172.18.0.1";
    private final AuthService authService;

    public AdminFilter(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LOGGER.info("do filter...");
        String token = ((HttpServletRequest )request).getHeader(ADMIN_AUTHORIZATION);
        String remote = ((HttpServletRequest )request).getHeader(REMOTE_HOST);

        if(WHITE_IP.equals(request.getRemoteAddr()) || WHITE_IP.equals(remote)) {
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(null, null, null);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }

        if(token != null && authService.checkLineInFile(token, AuthService.adminAccessTokensFile)) {
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(null, null, null);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }

        chain.doFilter(request, response);
    }
}
