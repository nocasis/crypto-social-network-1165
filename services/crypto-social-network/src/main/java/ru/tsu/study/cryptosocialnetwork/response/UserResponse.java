package ru.tsu.study.cryptosocialnetwork.response;

import ru.tsu.study.cryptosocialnetwork.model.Post;
import ru.tsu.study.cryptosocialnetwork.model.User;

import java.util.List;

public class UserResponse {

    private User user;
    private List<Post> posts;
    private String status;
    private String message;

    public UserResponse(User user, List<Post> posts) {
        this.user = user;
        this.posts = posts;
        this.status = "ok";
        this.message = "";
    }

    public UserResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
