package ru.tsu.study.cryptosocialnetwork.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tsu.study.cryptosocialnetwork.request.AdminAuthRequest;
import ru.tsu.study.cryptosocialnetwork.request.LoginRequest;
import ru.tsu.study.cryptosocialnetwork.request.RegistrationRequest;
import ru.tsu.study.cryptosocialnetwork.response.AdminAuthResponse;
import ru.tsu.study.cryptosocialnetwork.response.AuthResponse;
import ru.tsu.study.cryptosocialnetwork.service.AuthService;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/auth")
public class AuthController {

    final static Logger LOGGER = LogManager.getLogger(AuthController.class);
    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @RequestMapping()
    public ModelAndView main() {
      ModelAndView modelAndView = new ModelAndView();
      modelAndView.setViewName("index");
      return modelAndView;
    }

    @RequestMapping("/registerForm")
    public ModelAndView register() {
      ModelAndView modelAndView = new ModelAndView();
      modelAndView.setViewName("registration");
      return modelAndView;
    }

    @RequestMapping("/loginForm")
    public ModelAndView login() {
      ModelAndView modelAndView = new ModelAndView();
      modelAndView.setViewName("login");
      return modelAndView;
    }


    @PostMapping(path = "/registration", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthResponse> registration(@RequestBody RegistrationRequest registrationRequest) {
        LOGGER.info(registrationRequest);
        return authService.register(registrationRequest);
    }

    @PostMapping(path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthResponse> login(@RequestBody LoginRequest loginRequest) {
        LOGGER.info(loginRequest);
        return authService.login(loginRequest);
    }

    @GetMapping(path = "/admin/auth/start", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AdminAuthResponse> startAuthAdmin() {
        return authService.startAdminAuth();
    }

    @PostMapping(path = "/admin/auth/finish", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AdminAuthResponse> finishAuthAdmin(@RequestBody AdminAuthRequest adminAuthRequest) {
        LOGGER.info(adminAuthRequest);
        return authService.finishAdminAuth(adminAuthRequest);
    }

}
