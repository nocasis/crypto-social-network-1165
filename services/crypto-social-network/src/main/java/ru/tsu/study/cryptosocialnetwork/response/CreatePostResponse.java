package ru.tsu.study.cryptosocialnetwork.response;

public class CreatePostResponse {
    private String status;
    private String message;
    private Long postId;


    public CreatePostResponse(String status, Long postId) {
        this.status = status;
        this.message = "";
        this.postId = postId;
    }

    public CreatePostResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public CreatePostResponse(String status) {
        this.status = status;
        this.message = "";
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
