package ru.tsu.study.cryptosocialnetwork.repository;

import org.springframework.data.repository.CrudRepository;
import ru.tsu.study.cryptosocialnetwork.model.Post;

public interface PostRepository extends CrudRepository<Post, Long>, PostRepositoryCustom {
}
