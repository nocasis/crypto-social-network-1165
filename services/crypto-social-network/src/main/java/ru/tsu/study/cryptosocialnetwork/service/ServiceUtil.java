package ru.tsu.study.cryptosocialnetwork.service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ServiceUtil {

    @Value("${avatar.image.dir}")
    public String imageDir;
    private static final Logger LOGGER = LogManager.getLogger(ServiceUtil.class);

    public static List<Long> getIdsFromString(String ids) {
        List<Long> list = new ArrayList<Long>();
        for (String s : ids.split(","))
            if (!s.isEmpty()) list.add(Long.parseLong(s));
        return list;
    }


    public String saveSvg(InputStream is, String filename) throws IOException {
        File theDir = new File(imageDir);
        if (!theDir.exists()){
            theDir.mkdirs();
        }
        String[] path = filename.split("/");
        String filepath = theDir.getPath() + "/" + path[path.length-1];

        File outfile = new File(filepath);
        outfile.createNewFile();
        OutputStream ostream = new FileOutputStream(outfile);
        try {
            saveSVGasPNG(is, ostream);
        }
        catch(Exception e)
        {
            LOGGER.error(e);
        }
        return filepath;
    }

    public String saveImage(byte[] data, String filename) throws IOException {
        File theDir = new File(imageDir);
        if (!theDir.exists()){
            theDir.mkdirs();
        }
        String[] path = filename.split("/");
        String filepath = theDir.getPath() + "/" + path[path.length-1];
        File ava = new File(filepath);
        FileUtils.writeByteArrayToFile(ava, data);

        return filepath;
    }

    public static void saveSVGasPNG(InputStream svg, OutputStream outputStream) throws IOException {
        Transcoder transcoder = new PNGTranscoder();

        TranscoderInput input = new TranscoderInput(svg);
        TranscoderOutput output = new TranscoderOutput(outputStream);

        try {
            transcoder.transcode(input, output);
        } catch (TranscoderException  e) {
            LOGGER.error(e);
            throw new IOException(e);
        }
    }
}
