package ru.tsu.study.cryptosocialnetwork.security;

import io.jsonwebtoken.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

@Component
public class JwtProvider {

    @Value("${jwt.secret}")
    private String jwtSecret;

    private static final Logger LOGGER = LogManager.getLogger(JwtProvider.class);

    public String generateToken(Long id, String login, String publicKey) {
        Date date = Date.from(LocalDateTime.now().plusMinutes(30).toInstant(ZoneOffset.UTC));
        return Jwts.builder()
                .claim("user_id", id)
                .claim("login", login)
                .claim("public_key", publicKey)
                .setExpiration(date)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public String getLoginFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
        return claims.get("login").toString();
    }

    public String getUserIdFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
        return claims.get("user_id").toString();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
            return true;
        } catch (ExpiredJwtException expEx) {
            LOGGER.warn("Token expired");
        } catch (UnsupportedJwtException unsEx) {
            LOGGER.warn("Unsupported jwt");
        } catch (MalformedJwtException mjEx) {
            LOGGER.warn("Malformed jwt");
        } catch (SignatureException sEx) {
            LOGGER.warn("Invalid signature in jwt");
        } catch (Exception e) {
            LOGGER.warn("invalid token");
        }
        return false;
    }
}
