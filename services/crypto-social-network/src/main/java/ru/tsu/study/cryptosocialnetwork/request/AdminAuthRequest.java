package ru.tsu.study.cryptosocialnetwork.request;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

public class AdminAuthRequest {

    @JsonProperty("secret")
    private String secret;

    @JsonGetter("secret")
    public String getSecret() {
        return secret;
    }

    @JsonSetter("secret")
    public void setSecret(String secret) {
        this.secret = secret;
    }


    public String toString() {
        return "AdminAuthRequest = {secret: " + secret + "}";
    }
}
