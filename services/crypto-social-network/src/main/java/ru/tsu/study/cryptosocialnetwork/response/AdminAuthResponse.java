package ru.tsu.study.cryptosocialnetwork.response;

public class AdminAuthResponse {
    private String status;
    private String message;
    private String encryptedSecret;


    public AdminAuthResponse(String status, String message) {
        this.status = status;
        this.message = message;
        this.encryptedSecret = "";
    }

    public AdminAuthResponse(String status, String message, String encryptedSecret) {
        this.status = status;
        this.message = message;
        this.encryptedSecret = encryptedSecret;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public String getEncryptedSecret() {
        return encryptedSecret;
    }

    public void setEncryptedSecret(String encryptedSecret) {
        this.encryptedSecret = encryptedSecret;
    }
}
