package ru.tsu.study.cryptosocialnetwork.repository;

import ru.tsu.study.cryptosocialnetwork.model.Post;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


public class PostRepositoryImpl implements PostRepositoryCustom{

    @PersistenceContext
    EntityManager em;

    @Override
    public List<Post> findAllUserPublicPosts(Long ownerId) {
        String nativeQuery = "SELECT * FROM posts WHERE owner_id = " + ownerId.toString() + " AND is_private=false";
        Query query = em.createNativeQuery(nativeQuery, Post.class);
        List<Post> posts = new ArrayList<Post>();
        for(Post post: (List<Post>) query.getResultList()) {
            posts.add(new Post(post.getPostId(), ownerId, post.isPrivate(), post.getTheme(), post.getText(), post.getReportsCount()));
        }
        return posts;
    }
    @Override
    public List<Post> findAllUserPrivatePosts(Long ownerId) {
        String nativeQuery = "SELECT * FROM posts WHERE owner_id = " + ownerId.toString() + " AND is_private=true";
        Query query = em.createNativeQuery(nativeQuery, Post.class);
        List<Post> posts = new ArrayList<Post>();
        for(Post post: (List<Post>) query.getResultList()) {
            posts.add(new Post(post.getPostId(), ownerId, post.isPrivate(), post.getReportsCount()));
        }
        return posts;
    }

    @Override
    public Post reportPost(Long id) {
        String nativeQuery = "SELECT * FROM posts WHERE post_id = " + id.toString();

        Query query = em.createNativeQuery(nativeQuery, Post.class);
        Post post = (Post) query.getSingleResult();

        Long reportsCount = post.getReportsCount();
        post.setReportsCount(reportsCount + 1L);
        return post;
    }

    @Override
    public Post getReportedPost(Long id) {
        String nativeQuery = "SELECT * FROM posts WHERE post_id = " + String.valueOf(id) + " AND reports_count>4";

        Query query = em.createNativeQuery(nativeQuery, Post.class);

        return (Post) query.getSingleResult();
    }

    @Override
    public List<Post> getAllReportedPost(Long limit) {
        String nativeQuery = "SELECT * FROM posts WHERE reports_count>4 ORDER BY post_id DESC LIMIT " + limit;
        Query query = em.createNativeQuery(nativeQuery, Post.class);
        List<Post> posts = new ArrayList<Post>();
        posts.addAll((List<Post>) query.getResultList());
        return posts;
    }
}
