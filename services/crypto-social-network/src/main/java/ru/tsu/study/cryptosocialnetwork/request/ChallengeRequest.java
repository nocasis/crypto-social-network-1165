package ru.tsu.study.cryptosocialnetwork.request;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

public class ChallengeRequest {
    @JsonProperty("challenge")
    private String challenge;
    @JsonProperty("secret")
    private String secret;

    @JsonGetter("challenge")
    public String getChallenge() {
        return challenge;
    }

    @JsonSetter("challenge")
    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    @JsonGetter("secret")
    public String getSecret() {
        return secret;
    }

    @JsonSetter("secret")
    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String toString() {
        return "ChallengeRequest = {challenge: " + challenge + ", secret: " + secret + "}";
    }
}
