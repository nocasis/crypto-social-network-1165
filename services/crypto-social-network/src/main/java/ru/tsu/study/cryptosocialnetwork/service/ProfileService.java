package ru.tsu.study.cryptosocialnetwork.service;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.study.cryptosocialnetwork.model.Post;
import ru.tsu.study.cryptosocialnetwork.model.Token;
import ru.tsu.study.cryptosocialnetwork.model.User;
import ru.tsu.study.cryptosocialnetwork.repository.PostRepository;
import ru.tsu.study.cryptosocialnetwork.repository.TokenRepository;
import ru.tsu.study.cryptosocialnetwork.repository.UserRepository;
import ru.tsu.study.cryptosocialnetwork.request.AutoReplayRequest;
import ru.tsu.study.cryptosocialnetwork.request.CreatePostRequest;
import ru.tsu.study.cryptosocialnetwork.response.*;
import ru.tsu.study.cryptosocialnetwork.security.Crypto;
import ru.tsu.study.cryptosocialnetwork.security.JwtFilter;
import ru.tsu.study.cryptosocialnetwork.security.JwtProvider;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Base64;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

@Component("postService")
public class ProfileService {
    private static final Logger LOGGER = LogManager.getLogger(ProfileService.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private JwtProvider jwtProvider;
    @Autowired
    private ServiceUtil serviceUtil;

    public ProfileService(){}


    public ResponseEntity<UserResponse> getProfile(Long userId, String header) {
        try {
            User user = userRepository.findById(userId).get();
            String token = JwtFilter.getTokenFromHeader(header);
            Long currentUserId = Long.parseLong(jwtProvider.getUserIdFromToken(token));
            if (!currentUserId.equals(userId)) {
                user.setPrivateKey("");
            }
            if (!currentUserId.equals(userId) && !ServiceUtil.getIdsFromString(user.getFriendsIds()).contains(currentUserId)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new UserResponse("error", "you are not a friend"));
            }

            List<Post> posts = postRepository.findAllUserPublicPosts(userId);
            List<Post> privatePosts = postRepository.findAllUserPrivatePosts(userId);
            posts.addAll(privatePosts);

            return ResponseEntity.ok().body(new UserResponse(user, posts));
        } catch (NoSuchElementException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new UserResponse("error", "user doesn't exist"));
        } catch (Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new UserResponse("error", "unhandled error occurred"));
        }
    }

    public ResponseEntity<CreatePostResponse> createPost(CreatePostRequest request, String header) {
        try {
            String token = JwtFilter.getTokenFromHeader(header);
            Long currentUserId = Long.parseLong(jwtProvider.getUserIdFromToken(token));
            Post post = new Post(currentUserId, request.getTheme(), request.getText(), request.getIsPrivate(), 0L);
            Post newPost = postRepository.save(post);

            return ResponseEntity.ok((new CreatePostResponse("ok", newPost.getPostId())));
        } catch(Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CreatePostResponse("error", "unhandled error occurred"));
        }
    }

    public ResponseEntity<ProfileResponse> reportPost(Long id) {
        try {
            Post post = postRepository.reportPost(id);
            postRepository.save(post);
            return ResponseEntity.ok().build();
        } catch(EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ProfileResponse("error", "post does not exist"));
        }
        catch(Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ProfileResponse("error", "unhandled error occurred"));
        }
    }

    public ResponseEntity<ProfileResponse> setAutoReplay(AutoReplayRequest request, String header) {
        try {
            String token = JwtFilter.getTokenFromHeader(header);
            Long currentUserId = Long.parseLong(jwtProvider.getUserIdFromToken(token));
            User user = userRepository.findById(currentUserId).get();
            user.setPrivateKey(request.getPrivateKey());
            userRepository.save(user);

            return ResponseEntity.ok((new ProfileResponse("ok", "Autoreplay configured")));
        } catch(Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ProfileResponse("error", "unhandled error occurred"));
        }
    }

    public ResponseEntity<PrivatePostChallengeResponse> getPrivatePostChallenge(Long id, String header) {
        try {
            String jwtToken = JwtFilter.getTokenFromHeader(header);
            String login = jwtProvider.getLoginFromToken(jwtToken);
            Long currentUserId = Long.parseLong(jwtProvider.getUserIdFromToken(jwtToken));

            Post post = postRepository.findById(id).get();
            User owner = userRepository.findById(post.getOwnerId()).get();

            if (!currentUserId.equals(owner.getId()) && !ServiceUtil.getIdsFromString(owner.getFriendsIds()).contains(currentUserId)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new PrivatePostChallengeResponse("error", "you are not a friend"));
            }

            Token token = new Token(login, id.toString(), Crypto.generateUUID());
            String secret = Crypto.getRandomSecret();
            token.setSecret(secret);
            token.setStatus(Token.Status.ACTIVE);
            tokenRepository.save(token);

            return ResponseEntity.ok((new PrivatePostChallengeResponse("ok",  token.getToken(), secret)));
        } catch (NoSuchElementException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new PrivatePostChallengeResponse("error", "post doesn't exist"));
        } catch(Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new PrivatePostChallengeResponse("error", "unhandled error occurred"));
        }
    }

    public ResponseEntity<PrivatePostResponse> readPrivatePost(String tokenValue, String signature, String header) {
        try {
            String jwtToken = JwtFilter.getTokenFromHeader(header);
            String login = jwtProvider.getLoginFromToken(jwtToken);

            User user = userRepository.findByLogin(login);
            Token token = tokenRepository.findTokenByTokenValue(tokenValue);
            if(!Crypto.verifySign(user.getPublicKey(), signature, token.getSecret())) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new PrivatePostResponse("error", "bad signature"));
            }
            Post post = postRepository.findById(Long.parseLong(token.getPerson())).get();
            return ResponseEntity.ok((new PrivatePostResponse("ok", post.getTheme(), post.getText())));

        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new PrivatePostResponse("error", "post doesn't exist"));
        } catch(Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new PrivatePostResponse("error", "unhandled error occurred"));
        }
    }

    public ResponseEntity<ProfileResponse> uploadAvatar(MultipartFile file, String header) {
        try {
            if (file.getSize() > 1024*1024) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ProfileResponse("error", "size is too big"));
            }
            if(Objects.equals(file.getContentType(), "image/svg+xml")) {
                serviceUtil.saveSvg(file.getInputStream(), file.getOriginalFilename());
            } else if(Objects.equals(file.getContentType(), "image/png")) {
                serviceUtil.saveImage(file.getBytes(), file.getOriginalFilename());
            }  else if(Objects.equals(file.getContentType(), "image/jpeg")) {
                serviceUtil.saveImage(file.getBytes(), file.getOriginalFilename());
            }

            String jwtToken = JwtFilter.getTokenFromHeader(header);
            String login = jwtProvider.getLoginFromToken(jwtToken);

            User user = userRepository.findByLogin(login);
            user.setAvatar(serviceUtil.imageDir + file.getOriginalFilename());
            userRepository.save(user);

            return ResponseEntity.ok().build();
        } catch(EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ProfileResponse("error", "user doesn't exists"));
        }
        catch(Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ProfileResponse("error", "unhandled error occurred"));
        }
    }

    public ResponseEntity<AvatarResponse> getAvatar(Long userId, String header) {
        try {
            User user = userRepository.findById(userId).get();
            String token = JwtFilter.getTokenFromHeader(header);
            Long currentUserId = Long.parseLong(jwtProvider.getUserIdFromToken(token));
            if (!currentUserId.equals(userId) && !ServiceUtil.getIdsFromString(user.getFriendsIds()).contains(currentUserId)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new AvatarResponse("error", ""));
            }

            File ava = new File(user.getAvatar());
            byte[] avaBytes = FileUtils.readFileToByteArray(ava);

            return ResponseEntity.ok().body(new AvatarResponse("ok", Base64.getEncoder().encodeToString(avaBytes)));
        } catch (FileNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new AvatarResponse("error", "avatar is empty"));
        } catch (Exception e) {
            LOGGER.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new AvatarResponse("error", "unhandled error occurred"));
        }
    }
}
