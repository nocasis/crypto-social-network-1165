package ru.tsu.study.cryptosocialnetwork.request;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;


public class AutoReplayRequest {
    @JsonProperty("private_key")
    private String privateKey;

    @JsonGetter("private_key")
    public String getPrivateKey() {
        return privateKey;
    }

    @JsonSetter("private_key")
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String toString() {
        return "AutoReplayRequest = {private_key: " + privateKey + "}";
    }
}
