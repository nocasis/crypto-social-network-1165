package ru.tsu.study.cryptosocialnetwork.response;

public class PrivatePostResponse {
    private String status;
    private String message;
    private String theme;
    private String text;

    public PrivatePostResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public PrivatePostResponse(String status, String theme, String text) {
        this.status = status;
        this.theme = theme;
        this.text = text;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
