package ru.tsu.study.cryptosocialnetwork.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.tsu.study.cryptosocialnetwork.model.Token;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class TokenRepositoryImpl implements TokenRepositoryCustom{

    private static final Logger LOGGER = LogManager.getLogger(TokenRepositoryImpl.class);

    @PersistenceContext
    EntityManager em;

    @Override
    public List<Token> findAllTokensByPerson(String person, Long limit) {
        String nativeQuery = "SELECT * FROM challenge_tokens WHERE person = '" + person + "' ORDER BY token_id DESC LIMIT " + limit;
        Query query = em.createNativeQuery(nativeQuery, Token.class);
        List<Token> tokens = new ArrayList<Token>();
        for(Token token: (List<Token>) query.getResultList()) {
            tokens.add(token);
        }
        return tokens;
    }

    @Override
    public Token findTokenByTokenValue(String token) {
        String nativeQuery = "SELECT * FROM challenge_tokens WHERE token='" + token + "'";
        Query query = em.createNativeQuery(nativeQuery, Token.class);

        return (Token) query.getSingleResult();
    }
}
