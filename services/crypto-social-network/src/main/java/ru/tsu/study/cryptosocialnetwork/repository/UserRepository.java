package ru.tsu.study.cryptosocialnetwork.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.tsu.study.cryptosocialnetwork.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>, UserRepositoryCustom {

}
