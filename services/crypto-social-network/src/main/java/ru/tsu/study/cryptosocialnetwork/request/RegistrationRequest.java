package ru.tsu.study.cryptosocialnetwork.request;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;


public class RegistrationRequest {
    @JsonProperty("login")
    private String login;
    @JsonProperty("public_Key")
    private String publicKey;

    @JsonGetter("login")
    public String getLogin() {
        return login;
    }

    @JsonSetter("login")
    public void setLogin(String login) {
        this.login = login;
    }

    @JsonGetter("public_key")
    public String getPublicKey() {
        return publicKey;
    }

    @JsonSetter("public_key")
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }


    public String toString() {
        return "RegistrationRequest = {login: " + login + ", public_key: " + publicKey + "}";
    }
}
