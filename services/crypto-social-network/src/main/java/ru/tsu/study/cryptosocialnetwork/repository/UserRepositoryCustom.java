package ru.tsu.study.cryptosocialnetwork.repository;

import org.springframework.data.repository.NoRepositoryBean;
import ru.tsu.study.cryptosocialnetwork.model.User;

import java.util.List;

@NoRepositoryBean
public interface UserRepositoryCustom {

    User findByLogin(String login);
    boolean userExistsByLogin(String login);
    List<User> findAllUsers(Long limit);
}
