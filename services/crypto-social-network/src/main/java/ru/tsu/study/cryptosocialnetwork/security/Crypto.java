package ru.tsu.study.cryptosocialnetwork.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.crypto.codec.Hex;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.*;
import java.util.*;

public class Crypto {

    private static final Logger LOGGER = LogManager.getLogger(Crypto.class);

    public static Boolean validateSign(String publicKey, String sign) {
        return true;
    }

    public static String generateUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    public static void generateSign() {
        try {
            ECGenParameterSpec ecSpec = new ECGenParameterSpec("secp256r1");
            KeyPairGenerator g = KeyPairGenerator.getInstance("EC");
            g.initialize(ecSpec, new SecureRandom());
            KeyPair keypair = g.generateKeyPair();
            PublicKey publicKey = keypair.getPublic();
            PrivateKey privateKey = keypair.getPrivate();

            Signature ecdsaSign = Signature.getInstance("SHA256withECDSA");
            ecdsaSign.initSign(privateKey);
            ecdsaSign.update("plaintext".getBytes(StandardCharsets.UTF_8));
            byte[] signature = ecdsaSign.sign();
            String pub = new String(Hex.encode(publicKey.getEncoded()));
            String priv = new String(Hex.encode(privateKey.getEncoded()));
            String sig = new String(Hex.encode(signature));
            System.out.println(pub);
            System.out.println(priv);
            System.out.println(sig);

            System.out.println(verifySign(pub, sig, "plaintext"));

        } catch(Exception e) {
            System.out.println(e);
        }
    }

    public static boolean verifySign(String publicKeyString, String signature, String data) {
        try{
            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(Hex.decode(publicKeyString));
            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

            Signature ecdsaSign = Signature.getInstance("SHA256withECDSA");
            ecdsaSign.initVerify(publicKey);
            ecdsaSign.update(data.getBytes(StandardCharsets.UTF_8));
            return ecdsaSign.verify(Hex.decode(signature));
        } catch(Exception e) {
            LOGGER.error(e);
            return false;
        }
    }

    public static byte[] doKeyAgree(String alicePrivate, String bobPublic) {
        try{
            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(Hex.decode(bobPublic));
            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

            EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(Hex.decode(alicePrivate));
            PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

            return doKeyAgree(privateKey, publicKey);
        } catch(Exception e) {
            LOGGER.error(e);
            return null;
        }
    }

    public static byte[] doKeyAgree(PrivateKey alicePrivate, PublicKey bobPublic) {
        try {
            KeyAgreement keyAgree = KeyAgreement.getInstance("ECDH");
            keyAgree.init(alicePrivate);
            keyAgree.doPhase(bobPublic, true);
            byte[] sharedSecret = keyAgree.generateSecret();
            return sha256(sharedSecret);
        } catch(Exception e) {
            LOGGER.error(e);
            return null;
        }
    }

    public static byte[] sha256(byte[] data) {
        try {
            MessageDigest hash = MessageDigest.getInstance("SHA-256");
            hash.update(data);
            return hash.digest();
        } catch(Exception e) {
            LOGGER.error(e);
            return null;
        }
    }

    public static byte[] md5(byte[] data) {
        try {
            MessageDigest hash = MessageDigest.getInstance("MD5");
            hash.update(data);
            return hash.digest();
        } catch(Exception e) {
            LOGGER.error(e);
            return null;
        }
    }


    public static String aes(String data, byte[] key) {
        try{
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);
            return Base64.getEncoder().encodeToString(cipher.doFinal(data.getBytes(StandardCharsets.UTF_8)));
        } catch(Exception e) {
            LOGGER.error(e);
            return null;
        }
    }

    public static String getRandomSecret() {
        try {
            ByteArrayOutputStream secret = new ByteArrayOutputStream();

            Random random = new Random();
            int randomInt = random.nextInt(4);

            long dataLong = new Date().getTime() / 1000L;
            byte[] data = longToBytes(dataLong);
            byte[] sha256Bytes = Arrays.copyOfRange(sha256(data),0,8);
            byte[] md5Bytes = Arrays.copyOfRange(md5(longToBytes(dataLong*randomInt)), 12, 16);

            secret.write(sha256Bytes);
            secret.write(md5Bytes);

            return new String(Hex.encode(secret.toByteArray()));
        } catch (IOException e) {
            LOGGER.error(e);
            return null;
        }
    }

    public static byte[] doKeyAgreeWithAdmin(String adminPublic) {
        try{
            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(Hex.decode(adminPublic));
            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            PublicKey adminPublicKey = keyFactory.generatePublic(publicKeySpec);

            ECGenParameterSpec ecSpec = new ECGenParameterSpec("secp256r1");
            KeyPairGenerator g = KeyPairGenerator.getInstance("EC");
            g.initialize(ecSpec, new SecureRandom());
            KeyPair keypair = g.generateKeyPair();
            PublicKey publicKey = keypair.getPublic();
            PrivateKey privateKey = keypair.getPrivate();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(Objects.requireNonNull(doKeyAgree(privateKey, adminPublicKey)));
            baos.write(publicKey.getEncoded());

            return baos.toByteArray();

        } catch(Exception e) {
            LOGGER.error(e);
            return null;
        }
    }

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }
}
