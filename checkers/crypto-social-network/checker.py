#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import inspect
import os
import sys
from enum import Enum
from typing import Tuple
from Crypto.PublicKey import ECC
from Crypto.Signature import DSS
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
from cryptography.hazmat.primitives.asymmetric import ec
import base64
import urllib3

import requests
from json import load
from random import randrange, choice, seed
from string import ascii_letters, digits
from pathlib import Path
import re
import sqlite3


class CheckerDefaultValues:
    # SERVICE INFO
    PORT = 8090
    HOST = ""
    # DEBUG -- logs to stderr, TRACE -- verbose log
    DEBUG = os.getenv("DEBUG", True)
    TRACE = os.getenv("TRACE", False)
    # FILES
    ROOT_PATH = Path(__file__).parent.resolve()
    LOGIN_PATH = ROOT_PATH.joinpath("names")
    NAME_FILENAME = LOGIN_PATH.joinpath("names.json")
    SURNAME_FILENAME = LOGIN_PATH.joinpath("surnames.json")
    MAIL_FILENAME = LOGIN_PATH.joinpath("mails.json")
    POSTS_PATH = ROOT_PATH.joinpath("posts")
    POST_FILENAME = POSTS_PATH.joinpath("cves.json")
    IMAGES_PATH = ROOT_PATH.joinpath("pics")
    IMAGES_SVG = IMAGES_PATH.joinpath("cats.svg")
    IMAGES_CHANGED_SVG = IMAGES_PATH.joinpath("cats.png")
    IMAGES_JPG = IMAGES_PATH.joinpath("monkey.jpg")
    IMAGES_PNG = IMAGES_PATH.joinpath("wolf.png")
    # DATABASE
    DB_PATH = ROOT_PATH.joinpath("db")
    INFO_DB = DB_PATH.joinpath("info.db")
    USERS_DB = DB_PATH.joinpath("users.db")
    # OPTIONAL
    RANDOM_ALPHABET = ascii_letters + digits
    ADMIN_PRIVATE = (
        "308187020100301306072a8648ce3d020106082a8648ce3d030107046d306b0201010420"
        "29f96bf4a410c53abb60a3ee0310129a47e2bd174369cc9acd23cddfed0207b9a1440342"
        "00043746b4bc6d1c365b6acc4aff4db87c19dfc972b65b5feda39c0d49a3af4a851be1ac"
        "6f4ebd2794e8aa336c76fc6373db4fbd614d740015e01256b6567175cea9"
    )
    ADMIN_PUBLIC = (
        "3059301306072a8648ce3d020106082a8648ce3d030107034200043746b4bc6d1c365b6ac"
        "c4aff4db87c19dfc972b65b5feda39c0d49a3af4a851be1ac6f4ebd2794e8aa336c76fc63"
        "73db4fbd614d740015e01256b6567175cea9"
    )


def put(flag_id: str, flag: str, vuln: str):
    """
    Put flag on host
    :param flag_id: flag_id of user to put flag
    :param flag: flag to put on host
    :param vuln: type of vuln (1 is for public, 2 is for private)
    :return:
    """
    try:
        if vuln == "1":
            add_user_with_auto_reply()
            user = create_user(flag_id)
            user.token = login_request(
                create_session(), user.login, user.sign_line(user.login)
            )
            if not user.token:
                _log(f"couldn't log in while putting the flag")
                die(ExitStatus.MUMBLE)
            _log("logged in")
            add_user_image(user.token, user.uid)
            add_info_about_user(user)
            _log("added info about user")
            post = {
                "is_private": False,
                "theme": topics[randrange(len(topics))],
                "text": flag,
            }
            post_id = send_post(create_session(user.token), post)
            save_info_about_post(user.login, post_id, post["is_private"])
            send_several_random_posts(create_session(user.token))
            die(ExitStatus.OK, f"{user.login}")
        else:
            user = get_info_about_last_user_without_private_post()
            user.token = login_request(
                create_session(), user.login, user.sign_line(user.login)
            )
            if not user.token:
                _log(f"couldn't log in while putting the flag")
                die(ExitStatus.MUMBLE)
            post = {
                "is_private": True,
                "theme": topics[randrange(len(topics))],
                "text": flag,
            }
            post_id = send_post(create_session(user.token), post)
            save_info_about_post(user.login, post_id, post["is_private"])
            send_several_random_posts(create_session(user.token))
            die(ExitStatus.OK, f"{user.login}")
    except (requests.RequestException, urllib3.exceptions.ConnectionError) as e:
        _log(e)
        _log(f"request exception was caught: {e}")
        die(ExitStatus.DOWN)
    except Exception as e:
        _log(e)
        _log(f"Something went wrong with checker: {e}")
        die(ExitStatus.CHECKER_ERROR)


def get(flag_id: str, flag: str, vuln: str):
    """
    Get from flag_id provided flag in a vuln place
    """
    try:
        user = get_info_about_user(flag_id)
        token = login_request(create_session(), user.login, user.sign_line(user.login))
        if vuln == "1":
            if get_public_post(create_session(token), flag, user.uid, False):
                die(ExitStatus.OK, f"{user.login}")
            _log(f"Post wasn't found for {user} with {flag}")
            die(ExitStatus.CHECKER_CORRUPT)
        elif vuln == "2":
            if solve_challenge_for_private_post(create_session(token), flag, user, []):
                die(ExitStatus.OK, f"{user.login}")
            _log(f"Post wasn't found for {user} with {flag}")
            die(ExitStatus.CHECKER_CORRUPT)
    except (requests.RequestException, urllib3.exceptions.ConnectionError) as e:
        _log(e)
        _log(f"request exception was caught: {e}")
        die(ExitStatus.DOWN)
    except Exception as e:
        _log(e)
        _log(f"Something went wrong with checker: {e}")
        die(ExitStatus.CHECKER_ERROR)


def check():
    """
    Check functionality
    """
    try:
        user1 = create_user()
        if not user1.uid:
            _log(
                f"couldn't create user with {user1.login}, {user1.public_key}, {user1.private_key}, {user1.sign_line(user1.login)}")
            die(ExitStatus.MUMBLE)
        user2 = create_user()
        if not user2.uid:
            _log(
                f"couldn't create user with {user2.login}, {user2.public_key}, {user2.private_key}, {user2.sign_line(user2.login)}")
            die(ExitStatus.MUMBLE)
        user1.token = login_request(
            create_session(), user1.login, user1.sign_line(user1.login)
        )
        if not user1.token:
            _log(f"couldn't login {user1.login}")
            die(ExitStatus.MUMBLE)

        user2.token = login_request(
            create_session(), user2.login, user2.sign_line(user2.login)
        )

        if not user2.token:
            _log(f"couldn't login {user2.login}")
            die(ExitStatus.MUMBLE)

        check_get_login_by_id(create_session(user1.token), user1.uid, user1.login)
        check_get_login_by_id(create_session(user1.token), user2.uid, user2.login)

        if not check_adding_avatar(
                user1.token, user1.uid, CheckerDefaultValues.IMAGES_PNG
        ):
            _log("error occurred with uploading and checking picture with png type")
            die(ExitStatus.MUMBLE)

        if not check_adding_avatar(
                user1.token, user1.uid, CheckerDefaultValues.IMAGES_SVG
        ):
            _log("error occurred with uploading and checking picture with svg type")
            die(ExitStatus.MUMBLE)

        if not check_adding_avatar(
                user2.token, user2.uid, CheckerDefaultValues.IMAGES_JPG
        ):
            _log("error occurred with uploading and checking picture with jpg type")
            die(ExitStatus.MUMBLE)

        if not check_removing_token(user1.login, user2.login, user1.token, user2.token):
            _log("couldn't remove token")
            die(ExitStatus.MUMBLE)
        if check_requests_and_delete_token(create_session(user1.token), user1.login):
            _log("token wasn't removed")
            die(ExitStatus.MUMBLE)

        if not check_adding_friend(user1, user2):
            _log("adding friend isn't available")
            die(ExitStatus.MUMBLE)

        if not check_auto_reply_function(user1):
            _log("couldn't add auto reply user")
            die(ExitStatus.MUMBLE)

        if not check_view_of_users_at_all(
                create_session(user1.token), user1.login, user2.login
        ):
            _log("created users weren't found")
            die(ExitStatus.MUMBLE)

        if not check_view_of_friends(user1.uid, user2.uid, user1.token):
            _log("user wasn't found in friend's list")
            die(ExitStatus.MUMBLE)

        if not check_view_friend_public_post(user1.uid, user1.token, user2.token):
            _log("couldn't see friend's public post")
            die(ExitStatus.MUMBLE)

        if not check_view_friend_private_post(user1.token, user2):
            _log("couldn't see friend's private post")
            die(ExitStatus.MUMBLE)

        send_several_random_posts(create_session(user1.token))
        send_several_random_posts(create_session(user2.token))

        check_admin(user1.token, user2.token)
        die(ExitStatus.OK)
    except (requests.RequestException, urllib3.exceptions.ConnectionError) as e:
        _log(e)
        die(ExitStatus.DOWN)
    except Exception as e:
        _log(e)
        die(ExitStatus.CHECKER_ERROR)


def info():
    """
    Return info about number of the vulnerabilities with an order for the checker
    """
    print("vulns: 1:2", flush=True, file=sys.stdout, end="")
    die(ExitStatus.OK)


class User:
    def __init__(self):
        self.login: str or None = None
        self.private_key: str or None = None
        self.public_key: str or None = None
        self.uid: int or None = None
        self.is_trusted = False
        self.token: str or None = None

    # TODO: randomness
    def generate_user_login_by_flag_id(self, flag_id: str or None):
        """
        Generate random login using flag_id as seed
        :param flag_id: flag_id is given for user
        :return: login
        """
        if flag_id:
            seed(flag_id)
        name_ord = randrange(
            number_of_lines_in_table(CheckerDefaultValues.NAME_FILENAME)
        )
        sur_ord = randrange(
            number_of_lines_in_table(CheckerDefaultValues.SURNAME_FILENAME)
        )
        domain_ord = randrange(
            number_of_lines_in_table(CheckerDefaultValues.MAIL_FILENAME)
        )
        # _log(name_ord)
        # _log(sur_ord)
        # _log(domain_ord)
        username = get_names_from_db(name_ord)
        surname = get_surnames_from_db(sur_ord)
        mail = get_mails_from_db(domain_ord)
        login = username + "." + surname + str(randrange(1920, 2020)) + "@" + mail
        _log(login)
        self.login = login

    def generate_pair_of_keys(self):
        """
        Generate keypair for user
        """
        sk = ECC.generate(curve="P-256")
        pk = sk.public_key()
        self.private_key = sk.export_key(format="DER").hex()
        self.public_key = pk.export_key(format="DER").hex()
        _log(f"private_key: {self.private_key}\n public_key:{self.public_key}")

    def sign_line(self, for_signing: str):
        """
        Sign given line by private key
        :return: signed line
        """
        sk = ECC.import_key(bytes.fromhex(self.private_key))
        signer = DSS.new(sk, mode="deterministic-rfc6979", encoding="der")
        h = SHA256.new(for_signing.encode())
        signed_line = signer.sign(h)
        _log(signed_line.hex())
        return signed_line.hex()

    def create_user(self, flag_id: str or None):
        """
        Generate login and keypair
        """
        self.generate_user_login_by_flag_id(flag_id)
        self.generate_pair_of_keys()

    def fill_by_known(
            self, login: str, sk: str, pk: str, user_id: int, is_trusted: bool
    ):
        """
        Fill User() object's fields by provided values
        """
        self.login = login
        self.private_key = sk
        self.public_key = pk
        self.uid = user_id
        self.is_trusted = is_trusted


def create_session(token: str or None = None) -> requests.Session:
    """
    Create Session object for queries with provided headers
    """
    session = requests.session()
    session.headers.update(
        {
            "Content-Type": "application/json",
            "Connection": "close",
        }
    )
    if token:
        session.headers.update({"Authorization": f"Bearer {token}"})
    return session


def generate_random_secret(
        length: int, alphabet: str = CheckerDefaultValues.RANDOM_ALPHABET
) -> str:
    """
    Generate a sequence of the random symbols from the provided alphabet
    with the given length
    """
    return "".join(choice(alphabet) for _ in range(length))


def generate_random_post() -> str:
    """
    Get post by finding in DB by random number
    """
    number = randrange(number_of_lines_in_table(CheckerDefaultValues.POST_FILENAME))
    post = get_post_text_from_db(number)[:1024]
    return post


def create_user(flag_id: str or None = None) -> User:
    """
    Create user with login depending from flag_id
    :param flag_id: given random value
    :return: created user with whole information
    """
    user = User()
    user.create_user(flag_id)
    user.uid = register_request(create_session(), user.login, user.public_key)
    return user


def register_request(session: requests.Session, login: str, pk: str) -> int or None:
    """
    Request for registration
    :param session:
    :param login: user's login
    :param pk: user's public key
    :return: user_id taken from server
    """
    l0 = {"login": login, "public_key": pk}
    r = session.post(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/auth/registration",
        json=l0,
    )
    _log(r.text)
    if r.status_code == 400:
        return None
    elif r.status_code == 500:
        _log(f"occurred error in the registration of the user: {l0}")
        die(ExitStatus.MUMBLE)
    user_id = re.findall("[0-9]+", r.json()["message"])
    return int(user_id[0])


def login_request(
        session: requests.Session, login: str, signed_login: str
) -> str or None:
    """
    Request to log in using login and signed key
    :param session:
    :param login: user's login
    :param signed_login: login signed with private key
    :return: jwt token for authenticated operations
    """
    l0 = {"login": login, "signed_login": signed_login}
    r = session.post(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/auth/login",
        json=l0,
    )
    _log(r.text)
    if r.status_code == 400:
        return None
    elif r.status_code == 500:
        die(
            ExitStatus.MUMBLE,
            f"occurred error in the login request of the user: {l0}",
        )
    token = r.json()["message"]
    _log(token)
    return token


def add_user_image(token, user_id, image_type: Path or None = None):
    """
    Add image to user's profile
    :param token: auth token
    :param user_id: id of user for getting image
    :param image_type: type of uploading image if provided
    :return:
    """
    if not image_type:
        image_type = choice(
            [
                CheckerDefaultValues.IMAGES_JPG,
                CheckerDefaultValues.IMAGES_PNG,
                CheckerDefaultValues.IMAGES_SVG,
            ]
        )
    if image_type == CheckerDefaultValues.IMAGES_JPG:
        content_type = "image/jpeg"
    if image_type == CheckerDefaultValues.IMAGES_PNG:
        content_type = "image/png"
    if image_type == CheckerDefaultValues.IMAGES_SVG:
        content_type = "image/svg+xml"
    filename = image_type.name
    _log(filename)
    f0 = {"file": (filename, open(image_type, "rb"), content_type)}
    session = requests.Session()
    session.headers.update(
        {
            "Authorization": f"Bearer {token}",
            "Connection": "keep-alive",
        }
    )
    r = session.post(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/profile/upload/avatar",
        files=f0,
    )
    if r.status_code != 200:
        _log(r.text)
        _log("Couldn't send image")
        die(ExitStatus.MUMBLE)
    if not check_viewing_picture(session, user_id):
        _log("Couldn't check viewing picture")
        die(ExitStatus.MUMBLE)
    _log("succeeded with user's picture")


def check_viewing_picture(session: requests.Session, user_id: int):
    r = session.get(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/profile/avatar?id={user_id}"
    )
    if r.status_code != 200:
        _log(r.text)
        return False
    if len(r.json()["message"]) == 0:
        _log("there were no bytes of user's picture")
        die(ExitStatus.MUMBLE)
    return r.json()["message"]


def add_request_friend(session: requests.Session, login: str) -> str:
    """
    Post request for adding friend
    :param session:
    :param login: friend's login
    :return: token for approving challenge
    """
    l0 = {"login": login}
    r = session.post(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/users/friends/add?friendLogin={login}",
        json=l0,
    )
    _log(r.text)
    if r.status_code != 200:
        _log(f"occurred error in adding friend {login}: {r.text}")
        die(ExitStatus.MUMBLE)
    token = r.json()["token"]
    return token


def sha_ecdh(private: ECC.EccKey, public: ECC.EccKey) -> bytes:
    """
    Compute SHA256(ECDH(private, public))
    :return: shared key
    """
    sk = ec.EllipticCurvePrivateNumbers(
        int(private.d),
        ec.EllipticCurvePublicNumbers(
            int(private.pointQ.x), int(private.pointQ.y), ec.SECP256R1()
        ),
    ).private_key()
    pk = ec.EllipticCurvePublicNumbers(
        int(public.pointQ.x), int(public.pointQ.y), ec.SECP256R1()
    ).public_key()
    dh = sk.exchange(ec.ECDH(), pk)
    _log(f"dh:{dh.hex()}")
    key = SHA256.new(dh)
    _log(f"key:{key.hexdigest()}")
    return key.digest()


def create_challenge(secret: str, r_private: str, init_public: str) -> str:
    """
    Create challenge base64(AES_ECB(key=SHA256(ECDH(private_init, public_resp)), secret)))
    :param secret: secret for challenge
    :param r_private: responder's private key
    :param init_public: initiator's public key
    :return: encoded challenge
    """
    private = ECC.import_key(bytes.fromhex(r_private))
    public = ECC.import_key(bytes.fromhex(init_public))
    key = sha_ecdh(private, public)

    cipher = AES.new(key, mode=AES.MODE_ECB)
    encrypted = cipher.encrypt(pad(secret.encode(), 16))
    challenge = base64.b64encode(encrypted)
    return challenge.decode()


def solve_challenge(challenge: str, init_private: str, r_public: str) -> str:
    """
    Solve challenge base64(AES_ECB(key=SHA256(ECDH(private_init, public_resp)), secret)))
    :param challenge: encoded challenge
    :param init_private: initiator's private key
    :param r_public: responder's public key
    :return: decoded secret
    """
    private = ECC.import_key(bytes.fromhex(init_private))
    public = ECC.import_key(bytes.fromhex(r_public))
    key = sha_ecdh(private, public)

    encrypted = base64.b64decode(challenge.encode())
    cipher = AES.new(key, mode=AES.MODE_ECB)
    secret = cipher.decrypt(encrypted)
    return unpad(secret, 16).decode()


def get_and_solve_challenge(
        session: requests.Session, token, init_sk, resp_pk, login
) -> bool:
    """
    Get info about approved challenge and solve it
    :param session:
    :param token: token of the challenge
    :param init_sk: private key of the initiator
    :param resp_pk: public key of the responder
    :param login: responder's login
    :return: was challenge solved or not
    """
    r = session.get(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/users/challenge?token={token}"
    )
    if r.status_code != 200:
        _log(f"couldn't get challenge: {token}")
        _log(r.text)
        die(ExitStatus.MUMBLE)
    challenge = r.json()["challenge"]
    secret = solve_challenge(challenge, init_sk, resp_pk)
    solved = {"token": token, "challenge_answer": secret, "person": login}
    r = session.put(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/users/challenge/solve",
        json=solved,
    )
    if r.status_code != 200:
        _log(solved)
        _log(f"couldn't solve challenge with {solved}: {r.text}")
        die(ExitStatus.MUMBLE)
    return True


def check_requests_and_send_challenge(
        session: requests.Session, private, initiator, init_public
) -> str or None:
    """
    Get all requests and send challenge to chosen login
    :param session:
    :param private: responder's private key
    :param initiator: initiator's login
    :param init_public: initiator's public key
    :return:
    """
    r = session.get(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/users/friends/tokens?limit=100"
    )
    if r.status_code != 200:
        _log(r.text)
        _log(f"couldn't get requests from friends: {r.json()['status']}, {r.json()['message']}")
        die(ExitStatus.MUMBLE)
    init_request = next(
        init
        for init in r.json()
        if init["initiator"] == initiator and init["status"] == "NEW"
    )
    token = init_request["token"]
    random_str = generate_random_secret(24)
    challenge = create_challenge(random_str, private, init_public)
    ch = {"challenge": challenge, "secret": random_str}
    r = session.put(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/users/challenge?token={token}",
        json=ch,
    )
    if r.status_code != 200:
        _log(ch)
        _log(r.text)
        _log(f"couldn't put created challenge: {r.json()['status']}, {r.json()['message']}")
        die(ExitStatus.MUMBLE)


def delete_token(session: requests.Session, token: str):
    """
    Delete requested token for a friendship
    :return: is deleted
    """
    r = session.delete(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/users/friends/token?token={token}",
    )
    if r.status_code != 200:
        _log(r.json())
        return False
    return True


def check_requests_and_delete_token(
        session: requests.Session, login: str or None = None, token: str or None = None
) -> bool:
    """
    Delete token if provided or delete all new tokens from specified user or delete all new tokens
    :param session:
    :param login: specified initiator's login
    :param token: token for friendship (might be not provided)
    :return: was deleted or not
    """
    r = session.get(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/users/friends/tokens?limit=100",
    )
    if r.status_code != 200:
        _log(r.json())
        _log("couldn't get requests for challenges")
        die(ExitStatus.MUMBLE)
    if token:
        if not delete_token(session, token):
            return False
    elif login:
        init_request = list(
            init
            for init in r.json()
            if init["initiator"] == login and init["status"] == "NEW"
        )
        _log(f"init request: {init_request}")
        if len(init_request) == 0:
            return False
        for init in init_request:
            if not delete_token(session, init["token"]):
                return False
    else:
        for init in r.json():
            if init["status"] == "NEW":
                if not delete_token(session, init["token"]):
                    return False
    return True


def add_trusted_friend(session: requests.Session, init_sk) -> None:
    """
    Add friend who gave private key to a server
    :param session:
    :param init_sk: initiator's private key
    :return:
    """
    trusted_user = get_info_about_trusted_user()
    _log(f"trusted one is: {trusted_user.login}, {trusted_user.public_key}")
    token = add_request_friend(session, trusted_user.login)
    _log(f"token is {token}")
    get_and_solve_challenge(
        session, token, init_sk, trusted_user.public_key, trusted_user.login
    )


def add_auto_replay(session: requests.Session, sk: str) -> None:
    """
    Add auto replay to server with trusting private key to it
    :param session:
    :param sk: private key
    :return: None
    """
    auto_replay = {"private_key": sk}
    r = session.post(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/profile/autoReplay",
        json=auto_replay,
    )
    if r.status_code != 200:
        _log(r.text)
        _log("couldn't add auto replay")
        die(ExitStatus.MUMBLE)


def add_user_with_auto_reply():
    """
    Generate user with auto replay and save it
    """
    last_id = get_last_user_id()
    if last_id and last_id % 5 != 0:
        return
    user = create_user()
    user.token = login_request(create_session(), user.login, user.sign_line(user.login))
    add_auto_replay(create_session(user.token), user.private_key)
    user.is_trusted = True
    add_user_image(user.token, user.uid)
    send_several_random_posts(create_session(user.token))
    save_info_about_user(user)


def add_info_about_user(user: User) -> None:
    """
    Add info about user in db
    :param user:
    :return:
    """
    user.is_trusted = False
    add_trusted_friend(create_session(user.token), user.private_key)
    save_info_about_user(user)


def send_post(session: requests.Session, post: dict) -> int or None:
    """
    Post request for creating post
    """
    url = f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/profile/post"
    r = session.post(url, json=post)
    if r.status_code != 200:
        _log(f"couldn't send post: {r.text}")
        _log(post)
        die(ExitStatus.MUMBLE)
    post_id = r.json()["postId"]
    return post_id


def send_several_random_posts(session: requests.Session):
    """
    Send several posts from DB by user
    """
    post = {
        "is_private": False,
        "theme": topics[randrange(len(topics))],
        "text": "",
    }
    for _ in range(randrange(5)):
        post["text"] = generate_random_post()
        send_post(session, post)


def get_public_post(
        session: requests.Session, post: str, user_id: int, is_private: bool
) -> bool:
    """
    Get request for finding post with given flag
    :param session:
    :param post: given flag
    :param user_id: user's id given by the server
    :param is_private: post is public so value will be False
    :return:
    """
    url = f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/profile?id={user_id}"
    r = session.get(url)
    posts = r.json()["posts"]
    post_info = list(
        p for p in posts if p["private"] == is_private and p["text"] == post
    )
    if len(post_info) == 1:
        return True
    _log(post_info)
    return False


def solve_challenge_for_private_post(
        session: requests.Session, post: str, user: User, ids: list or None
) -> bool:
    """
    Send get requests for a challenge, solve it
    and find needed one among provided if there are several of them
    :param session:
    :param post: post (or a flag) that needed to be found
    :param user: user for signing incoming line
    :param ids: list of id or ids of post for finding needed one
    :return: is founded or not
    """
    if not ids:
        ids = get_info_about_post(user.login, True)
    _log(ids)
    for post_id in ids:
        url = f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/profile/post/private?id={post_id}"
        r = session.get(url)
        if r.status_code != 200:
            _log(r.text)
            _log(f'error with reading post: {r.json()["message"]}')
            die(ExitStatus.MUMBLE)
        token = r.json()["token"]
        secret = r.json()["secret"]
        signature = user.sign_line(secret)

        url = f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/profile/post/private/read?token={token}&signature={signature}"
        r = session.get(url)
        if r.status_code != 200:
            _log(r.text)
            _log(f'error with solving private post challenge: {r.json()["message"]}')
            die(ExitStatus.MUMBLE)
        if r.json()["text"] == post:
            return True
    return False


def check_get_login_by_id(session: requests.Session, user_id: int, login: str):
    """
    Check finding login by user's id
    """
    url = f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/users/{user_id}"
    r = session.get(url)
    if r.status_code != 200:
        _log(f"Couldn't find login by user's id {r.text}")
        die(ExitStatus.MUMBLE)
    if login != r.json()["login"]:
        _log(f"Couldn't get info about user by id: {r.text}")
        die(ExitStatus.MUMBLE)


def check_adding_avatar(token, uid, type_image) -> bool:
    """
    Check adding an image of provided type to the user
    :return: was added correctly or not
    """
    add_user_image(token, uid, type_image)
    if type_image == CheckerDefaultValues.IMAGES_SVG:
        type_image = CheckerDefaultValues.IMAGES_CHANGED_SVG
    with open(type_image, "rb") as fd:
        pic_bytes = base64.b64encode(fd.read())
    res = check_viewing_picture(create_session(token), uid)
    if res.encode() != pic_bytes:
        return False
    return True


def check_removing_token(l1, l2, token1, token2):
    """
    Check opportunity to remove incoming token
    :param l1: login of the first user
    :param l2: login of the second user
    :param token1: authentication token of the first user
    :param token2: authentication token of the second user
    :return: possible or not
    """
    token = add_request_friend(create_session(token1), l2)
    if check_requests_and_delete_token(create_session(token2), l1, token):
        return True
    return False


def check_adding_friend(
        user1: User,
        user2: User,
) -> bool:
    """
    Check opportunity to add another user as friend with the whole protocol
    :param user1:
    :param user2:
    :return: possible or not
    """
    token = add_request_friend(create_session(user1.token), user2.login)
    check_requests_and_send_challenge(
        create_session(user2.token),
        user2.private_key,
        user1.login,
        user1.public_key,
    )
    if get_and_solve_challenge(
            create_session(user1.token),
            token,
            user1.private_key,
            user2.public_key,
            user2.login,
    ):
        return True
    return False


def check_auto_reply_function(user: User) -> bool:
    """
    Create user with auto reply and check adding this friend
    """
    trusted_user = create_user()
    trusted_user.token = login_request(
        create_session(), trusted_user.login, trusted_user.sign_line(trusted_user.login)
    )
    if not check_adding_avatar(
            trusted_user.token, trusted_user.uid, CheckerDefaultValues.IMAGES_PNG
    ):
        _log("couldn't set picture for trusted user")
        die(ExitStatus.CHECKER_ERROR)
    add_auto_replay(create_session(trusted_user.token), trusted_user.private_key)
    token = add_request_friend(create_session(user.token), trusted_user.login)
    _log(f"trusted {trusted_user.login} is adding by {user.login} with {token}")
    if get_and_solve_challenge(
            create_session(user.token),
            token,
            user.private_key,
            trusted_user.public_key,
            trusted_user.login,
    ):
        _log(f"{trusted_user.login} was added by {user.login}")
        delete_user([trusted_user.uid])
        return True
    delete_user([trusted_user.uid])
    return False


def check_view_of_users_at_all(session: requests.Session, l1, l2, limit: int = 100):
    """
    Check possibility to view users in common list
    """
    r = session.get(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/users/?limit={limit}"
    )
    if r.status_code != 200:
        _log(f"Couldn't view all users : {r.text}")
        die(ExitStatus.MUMBLE)
    users = list(
        init for init in r.json() if init["login"] == l1 or init["login"] == l2
    )
    if len(users) != 2:
        _log(users)
        return False
    return True


def get_users_friends(session: requests.Session, uid) -> list or None:
    """
    Get user's friend's ids
    :param session: session
    :param uid: profile's id
    :return: list of friends
    """
    r = session.get(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/profile?id={uid}"
    )
    if r.status_code != 200:
        _log(f"Couldn't get user's friend's ids: {r.text}")
        die(ExitStatus.MUMBLE)
    friends_ids = list(int(i) for i in r.json()["user"]["friendsIds"].split(",")[:-1])
    return friends_ids


def check_view_of_friends(uid1, uid2, token1):
    """
    Check opportunity to see user's friend list from friend view
    """
    friends_ids = get_users_friends(create_session(token1), uid1)
    if not friends_ids:
        return False
    if uid2 not in friends_ids:
        return False
    friends_ids = get_users_friends(create_session(token1), uid2)
    if not friends_ids:
        return False
    if uid1 not in friends_ids:
        return False
    return True


def check_view_friend_public_post(uid1, token1, token2) -> bool:
    """
    Check if friend can view creator's public post
    :param uid1: friend's id
    :param token1: authentication token of the first user
    :param token2: authentication token of the second user
    :return: possible or not
    """
    secret = generate_random_post()
    post = {
        "is_private": False,
        "theme": topics[randrange(len(topics))],
        "text": secret,
    }
    post_id = send_post(create_session(token1), post)
    if not post_id:
        return False
    if get_public_post(create_session(token2), secret, uid1, False):
        return True
    return False


def check_view_friend_private_post(user1_token: str, user2: User) -> bool:
    """
    Create private post and check friend's opportunity to view it
    """
    secret = generate_random_post()
    post = {
        "is_private": True,
        "theme": topics[randrange(len(topics))],
        "text": secret,
    }
    post_id = send_post(create_session(user1_token), post)
    if solve_challenge_for_private_post(
            create_session(user2.token), secret, user2, [post_id]
    ):
        return True
    _log(f"secret is {secret}")
    _log(f"post is {post}")
    return False


def check_report_post(session: requests.Session, post_id: int) -> bool:
    """
    Check if post request to report post is succeed
    :param session:
    :param post_id: id of the post
    :return: possible or not
    """
    r = session.post(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/profile/post/report/{post_id}"
    )
    if r.status_code != 200:
        _log(f"Couldn't report post: {r.text}")
        die(ExitStatus.MUMBLE)
    return True


def login_admin(session: requests.Session) -> User or None:
    r = session.get(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/auth/admin/auth/start"
    )
    if r.status_code != 200:
        _log(f"Couldn't send request for becoming admin: {r.text}")
        die(ExitStatus.MUMBLE)
    challenge = r.json()["encryptedSecret"]
    server_public = r.json()["message"]
    admin = User()
    admin.private_key = CheckerDefaultValues.ADMIN_PRIVATE
    admin.public_key = CheckerDefaultValues.ADMIN_PUBLIC
    secret = solve_challenge(challenge, admin.private_key, server_public)
    admin_request = {"secret": secret}
    r = session.post(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/auth/admin/auth/finish",
        json=admin_request,
    )
    if r.status_code != 200:
        _log(f"Couldn't login as admin: {r.text}")
        die(ExitStatus.MUMBLE)
    admin.token = r.json()["encryptedSecret"]
    _log(admin.token)
    return admin


def check_admin_log(session: requests.Session):
    r = session.get(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/admin/log"
    )
    if r.status_code != 200:
        _log(f"Couldn't view logs: {r.text}")
        die(ExitStatus.MUMBLE)
    return True


def check_admin_all_reported(session: requests.Session, posts: list):
    r = session.get(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/admin/all/post/reported?limit=100"
    )
    # _log(r.json()[:10])
    if r.status_code != 200:
        _log(f"Couldn't get reported posts: {r.text}")
        die(ExitStatus.MUMBLE)
    for p in posts:
        if f'"postId":{p}' not in r.text:
            _log(f"{r.text}, {p}")
            return False
    return True


def generate_post_for_reporting(token: str, is_private: bool) -> Tuple[int, str]:
    """
    Generate random post (private or public) and send it
    """
    secret = generate_random_secret(24)
    post = {
        "theme": topics[randrange(len(topics))],
        "text": secret,
        "is_private": is_private,
    }
    post_id = send_post(create_session(token), post)
    return post_id, secret


def check_is_post_reported(
        token1: str, token2: str, admin_session: requests.Session, is_private: bool
) -> int or None:
    """
    Check that post can be viewed after 5 reports
    """
    post_id, secret = generate_post_for_reporting(token1, is_private)
    for i in range(5):
        if not check_report_post(create_session(token2), post_id):
            return False
    r = admin_session.get(
        f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/admin/post/reported?id={post_id}",
        json={"id": post_id},
    )
    if r.status_code != 200:
        _log(f"Couldn't view reportd posts: {r.text}")
        die(ExitStatus.MUMBLE)
    if secret == r.json()["text"]:
        _log(f"post_id:{post_id}")
        return post_id
    _log(r.text)
    return None


def delete_reported_post(admin_session: requests.Session, posts: list):
    """
    Delete provided reported posts
    """
    for post in posts:
        r = admin_session.delete(
            f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/admin/post?id={post}"
        )
        if r.status_code != 200:
            _log(f"{r.text} for {post}")
            return False
        if check_admin_all_reported(admin_session, [post]):
            return False
        if r.json()["message"] != "post deleted":
            return False
    return True


def delete_user(uids: list):
    """
    Delete user by provided id
    """
    admin = login_admin(create_session())
    admin_session = requests.session()
    admin_session.headers.update(
        {
            "Admin-Authorization": admin.token,
            "Connection": "keep-alive",
        }
    )
    for uid in uids:
        r = admin_session.delete(
            f"http://{CheckerDefaultValues.HOST}:{CheckerDefaultValues.PORT}/admin/user?id={uid}"
        )
        if r.status_code != 200:
            _log(f"Couldn't delete user by id{uid}: {r.text}")
            die(ExitStatus.MUMBLE)
            return False
    _log(f"{uid} was deleted")
    return True


def check_admin(user1_token: str, user2_token: str):
    """
    Check admin's functionality
    """
    _log(f"checking admin")
    admin = login_admin(create_session())
    admin_session = requests.session()
    admin_session.headers.update(
        {
            "Admin-Authorization": admin.token,
            "Connection": "keep-alive",
        }
    )
    if not check_admin_log(admin_session):
        die(ExitStatus.MUMBLE)

    pub_post = check_is_post_reported(user1_token, user2_token, admin_session, False)
    if not pub_post:
        _log(f"couldn't report public post")
        die(ExitStatus.MUMBLE)

    pr_post = check_is_post_reported(user1_token, user2_token, admin_session, True)
    if not pr_post:
        _log(f"couldn't report private post")
        die(ExitStatus.MUMBLE)

    if not check_admin_all_reported(admin_session, [pr_post, pub_post]):
        _log("couldn't view all reported posts by admin")
        die(ExitStatus.MUMBLE)

    if not delete_reported_post(admin_session, [pr_post, pub_post]):
        _log("couldn't delete all reported posts by admin")
        die(ExitStatus.MUMBLE)


# DB QUERIES
def get_info_about_user(login: str) -> User:
    """
    Get info about user(login, keys, user's id, true if user trusted to server) from file
    :param login: suppose to be login
    :return: User
    """
    db = sqlite3.connect(CheckerDefaultValues.USERS_DB)
    cursor = db.cursor()
    user = None
    try:
        cursor.execute(
            """SELECT * from users WHERE user_login=? AND host=?""",
            (login, CheckerDefaultValues.HOST),
        )
        result = cursor.fetchone()
        user = User()
        user.fill_by_known(result[1], result[2], result[3], result[4], result[5])
        db.close()
    except Exception as e:
        _log(f"{e}")
    return user


def get_last_user_id() -> int:
    """
    Get last number of user in provided host
    """
    db = sqlite3.connect(CheckerDefaultValues.USERS_DB)
    cursor = db.cursor()

    cursor.execute(
        """SELECT COUNT(pid) from users WHERE host=?""", (CheckerDefaultValues.HOST,)
    )
    result = cursor.fetchone()
    db.close()
    return result[0]


def get_info_about_trusted_user() -> User:
    """
    Get info about the last user that trusted the private key
    """
    db = sqlite3.connect(CheckerDefaultValues.USERS_DB)
    cursor = db.cursor()

    cursor.execute("""SELECT * FROM users WHERE host = ?""", (CheckerDefaultValues.HOST,))
    result = cursor.fetchall()
    # _log(result)

    cursor.execute(
        """SELECT * FROM users WHERE pid = (SELECT max(pid) from users WHERE is_trusted=1 AND host=?)""",
        (CheckerDefaultValues.HOST,),
    )
    result = cursor.fetchone()
    # _log(f"{result}, {type(result)}")
    user = User()
    user.fill_by_known(result[1], result[2], result[3], result[4], result[5])
    db.close()
    return user


def save_info_about_user(user: User):
    """
    Save info about user in DB
    """
    db = sqlite3.connect(CheckerDefaultValues.USERS_DB)
    cursor = db.cursor()

    query = """ INSERT INTO users(user_login, user_private, user_public, user_id, is_trusted, host)
                VALUES (?,?,?,?,?,?)
    """
    cursor.execute(
        query,
        (
            user.login,
            user.private_key,
            user.public_key,
            user.uid,
            user.is_trusted,
            CheckerDefaultValues.HOST,
        ),
    )
    db.commit()
    db.close()


def get_info_about_post(login, is_private):
    """
    Get info about posts from the particular user of the specified type
    """
    db = sqlite3.connect(CheckerDefaultValues.USERS_DB)
    cursor = db.cursor()

    cursor.execute(
        """SELECT post_id FROM posts WHERE user_login =  ? AND is_private=? AND host=?""",
        (
            login,
            is_private,
            CheckerDefaultValues.HOST,
        ),
    )
    result = cursor.fetchall()
    post_ids = list(i[0] for i in result)
    # _log(f"{post_ids}")
    db.close()
    return post_ids


def save_info_about_post(login, post_id, is_private):
    """
    Save info about post in DB
    """
    db = sqlite3.connect(CheckerDefaultValues.USERS_DB)
    cursor = db.cursor()

    query = """ INSERT INTO posts(user_login, post_id, is_private, host)
                    VALUES (?,?,?,?)
        """
    cursor.execute(
        query,
        (
            login,
            post_id,
            is_private,
            CheckerDefaultValues.HOST,
        ),
    )
    db.commit()
    db.close()


def find_user_with_provided_number_of_private_posts(cursor):
    """
    Find user who haven't send 2 private posts yet
    """
    try:
        cursor.execute(
            """SELECT user_login FROM posts WHERE pid=(SELECT max(pid) FROM posts WHERE is_private=? AND host=?)""",
            (True, CheckerDefaultValues.HOST,))
        login = cursor.fetchone()

        if not login:
            cursor.execute(
                """SELECT user_login FROM posts WHERE pid=(SELECT max(pid) FROM posts WHERE is_private=? AND host=?)""",
                (False, CheckerDefaultValues.HOST,))
            result = cursor.fetchone()
            return result[0]

        cursor.execute(
            """SELECT COUNT(pid) FROM posts WHERE user_login=? AND is_private=? AND host=?""",
            (login[0], True, CheckerDefaultValues.HOST,))
        result = cursor.fetchone()

        if result[0] < 2:
            return login[0]
        cursor.execute(
            """SELECT user_login FROM posts WHERE pid=(SELECT max(pid) FROM posts WHERE is_private=? AND host=?)""",
            (False, CheckerDefaultValues.HOST,))
        result = cursor.fetchone()
        return result[0]
    except Exception as e:
        _log(e)


def get_info_about_last_user_without_private_post():
    """
    Get user that haven't sent private post yet
    """
    db = sqlite3.connect(CheckerDefaultValues.USERS_DB)
    cursor = db.cursor()

    cursor.execute("""SELECT COUNT(pid) FROM users WHERE host = ?""", (CheckerDefaultValues.HOST,))
    pid_result = cursor.fetchall()
    result = None

    try:
        result = find_user_with_provided_number_of_private_posts(cursor)
    except Exception as e:
        _log(e)

    if result:
        cursor.execute("""SELECT * FROM users WHERE user_login = ?""", (result,))
    else:
        cursor.execute("""SELECT * FROM users WHERE pid = ?""", (pid_result,))
    result = cursor.fetchone()

    user = User()
    user.fill_by_known(result[1], result[2], result[3], result[4], result[5])
    db.close()
    return user


def get_names_from_db(pid):
    """
    Get a name from DB by provided id
    """
    db = sqlite3.connect(CheckerDefaultValues.INFO_DB)
    cursor = db.cursor()

    query = """ SELECT user_name FROM users_names WHERE pid=?"""
    cursor.execute(query, (pid,))
    result = cursor.fetchone()
    # _log(result)
    db.commit()
    db.close()
    return result[0]


def get_surnames_from_db(pid):
    """
    Get a surname from DB by provided id
    """
    db = sqlite3.connect(CheckerDefaultValues.INFO_DB)
    cursor = db.cursor()

    query = """ SELECT surname FROM surnames WHERE pid=?"""
    cursor.execute(query, (pid,))
    result = cursor.fetchone()
    # _log(result)
    db.commit()
    db.close()
    return result[0]


def get_mails_from_db(pid):
    """
    Get a mail from DB by provided id
    """
    db = sqlite3.connect(CheckerDefaultValues.INFO_DB)
    cursor = db.cursor()

    query = """ SELECT mail FROM mails WHERE pid=?"""
    cursor.execute(query, (pid,))
    result = cursor.fetchone()
    # _log(result)
    db.commit()
    db.close()
    return result[0]


def get_post_text_from_db(pid):
    """
    Get a text to post from DB by provided id
    """
    db = sqlite3.connect(CheckerDefaultValues.INFO_DB)
    cursor = db.cursor()
    # _log(f"pid is {pid}")

    query = """ SELECT post FROM post_texts WHERE pid=?"""
    cursor.execute(query, (pid,))
    result = cursor.fetchone()
    # _log(result)
    db.commit()
    db.close()
    return result[0]


file_to_table = {
    CheckerDefaultValues.NAME_FILENAME: """SELECT COUNT(pid) from users_names""",
    CheckerDefaultValues.SURNAME_FILENAME: """SELECT COUNT(pid) from surnames""",
    CheckerDefaultValues.MAIL_FILENAME: """SELECT COUNT(pid) from mails""",
    CheckerDefaultValues.POST_FILENAME: """SELECT COUNT(pid) from post_texts""",
}


def number_of_lines_in_table(file_table) -> int:
    """
    Get number of lines in provided table
    """
    db = sqlite3.connect(CheckerDefaultValues.INFO_DB)
    cursor = db.cursor()
    cursor.execute(file_to_table[file_table])
    result = cursor.fetchone()
    # _log(result)
    db.close()
    return result[0]


def fill_defaults():
    """
    Create tables with default values and fill them
    """
    db = sqlite3.connect(CheckerDefaultValues.INFO_DB)
    c = db.cursor()
    cmd = """
                CREATE TABLE IF NOT EXISTS
                    users_names(
                        pid INTEGER PRIMARY KEY AUTOINCREMENT,
                        user_name TEXT
                )"""
    c.execute(cmd)
    cmd = """
                        CREATE TABLE IF NOT EXISTS
                            surnames(
                                pid INTEGER PRIMARY KEY AUTOINCREMENT,
                                surname TEXT
                        )"""
    c.execute(cmd)
    cmd = """
                        CREATE TABLE IF NOT EXISTS
                            mails(
                                pid INTEGER PRIMARY KEY AUTOINCREMENT,
                                mail TEXT
                        )"""
    c.execute(cmd)
    cmd = """
                        CREATE TABLE IF NOT EXISTS
                            post_texts(
                                pid INTEGER PRIMARY KEY AUTOINCREMENT,
                                post TEXT
                        )"""
    c.execute(cmd)
    with open(CheckerDefaultValues.NAME_FILENAME, "r") as fd:
        names = load(fd)
    for i in names:
        query = """
                INSERT INTO users_names(pid, user_name)
                VALUES(?,?)
                """
        c.execute(query, (i, names[i]))
    with open(CheckerDefaultValues.SURNAME_FILENAME, "r") as fd:
        surnames = load(fd)
    for i in surnames:
        query = """
                INSERT INTO surnames(pid, surname)
                VALUES(?,?)
                """
        c.execute(query, (i, surnames[i]))
    with open(CheckerDefaultValues.MAIL_FILENAME, "r") as fd:
        mails = load(fd)
    for i in mails:
        query = """
                INSERT INTO mails(pid, mail)
                VALUES(?,?)
                """
        c.execute(query, (i, mails[i]))
    with open(CheckerDefaultValues.POST_FILENAME, "r") as fd:
        posts = load(fd)
    for i in posts:
        query = """
                INSERT INTO post_texts(pid, post)
                VALUES(?,?)
                """
        c.execute(query, (i, posts[i]))
    c.execute(cmd)
    db.commit()
    db.close()


def create_users_tables():
    """
    Create tables for user's info
    """
    db = sqlite3.connect(CheckerDefaultValues.USERS_DB)
    c = db.cursor()
    cmd = """
                        CREATE TABLE IF NOT EXISTS
                            users(
                                pid INTEGER PRIMARY KEY AUTOINCREMENT,
                                user_login TEXT,
                                user_private TEXT,
                                user_public TEXT,
                                user_id INTEGER,
                                is_trusted BOOL,
                                host TEXT
                        )"""
    c.execute(cmd)
    cmd = """
                        CREATE TABLE IF NOT EXISTS
                            posts(
                                pid INTEGER PRIMARY KEY AUTOINCREMENT,
                                user_login TEXT,
                                post_id INTEGER,
                                is_private BOOL,
                                host TEXT
                        )"""
    c.execute(cmd)
    db.commit()
    db.close()


def _create_and_check_db():
    """
    Create db for future info
    """
    if not os.path.exists(CheckerDefaultValues.DB_PATH):
        os.makedirs(CheckerDefaultValues.DB_PATH)
    if not os.path.exists(CheckerDefaultValues.INFO_DB):
        fill_defaults()
    if not os.path.exists(CheckerDefaultValues.USERS_DB):
        create_users_tables()


class ExitStatus(Enum):
    OK = 101
    CORRUPT = 102
    MUMBLE = 103
    DOWN = 104
    CHECKER_ERROR = 110


def _log(obj):
    if CheckerDefaultValues.DEBUG and obj:
        caller = inspect.stack()[1].function
        print(f"[{caller}] {obj}", file=sys.stderr, flush=True)
    return obj


def die(code: ExitStatus, msg: str or None = None):
    if msg:
        print(msg, file=sys.stdout, flush=True)
    exit(code.value)


def _main():
    _create_and_check_db()
    action, *args = sys.argv[1:]

    try:
        if action == "check":
            (CheckerDefaultValues.HOST,) = args
            check()
        elif action == "put":
            CheckerDefaultValues.HOST, flag_id, flag, vuln = args
            put(flag_id, flag, vuln)
        elif action == "get":
            CheckerDefaultValues.HOST, flag_id, flag, vuln = args
            get(flag_id, flag, vuln)
        elif action == "info":
            info()
        else:
            raise IndexError
    except ValueError:
        _log(ValueError)
        _log(f"Usage: {sys.argv[0]} check|put|get IP FLAGID FLAG")
        die(ExitStatus.CHECKER_ERROR)
    except Exception as e:
        _log(f"Exception: {e}. Stack:\n {inspect.stack()}")
        die(ExitStatus.CHECKER_ERROR)


topics = {
    0: "Startup",
    1: "Life",
    2: "Life Lessons",
    3: "Politics",
    4: "Travel",
    5: "Poetry",
    6: "Entrepreneurship",
    7: "Education",
    8: "Health",
    9: "Love",
    10: "Design",
    11: "Writing",
    12: "Technology",
    13: "Self Improvement",
    14: "Business",
    15: "Music",
    16: "Social Media",
    17: "Sports",
    18: "Food",
    19: "Art",
}

if __name__ == "__main__":
    _main()
